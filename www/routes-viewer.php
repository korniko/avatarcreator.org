<?php
return array(
	'still-jpg'=>array('GET|POST','[*:slug].jpg','controllers_View#mainStill'),
	'still-jpeg'=>array('GET|POST','[*:slug].jpeg','controllers_View#mainStill'),
	'animated-gif'=>array('GET|POST','[*:slug].gif','controllers_View#mainAnimated'),
	'info'=>array('GET','[*:slug].json','controllers_View#info'),
);
?>
