<?php
class model_categories extends ORM {
	var $id;
	var $name;
	var $total=0;
	var $order=0;
	var $format_id;
	var $created;
	var $parent_id;
	var $flags=0;
	var $isStart=false;
	var $avatarType; //Category exclusive to Type? NULL = NO

	var $position_category_id=0; //Parent position
	var $position=model_rolls::POSITION_ABOVE; //Use consts from roll

	//Used for layerMapping
	static $layerMap;
	static $zindex=0;

	//Category Flags
	const REQUIRED=1; //Category selection can not be left blank

	const CHILDREN_REQUIRED_ONLY=1;
	const CHILDREN_REQUIRED_FIRST=2;

	function __construct($id=NULL) {
		$c = Config::instance();
		$this->isStart = ($c['engine']['starting_category_id'] == $id);
		parent::loadFromID($id);
		if ($this->parent_id == $this->id) $this->parent_id = 0; //Infinity or Orphans?
	}

	static function all_categories() {
		$asset_table = ORM::getTable('model_assets');
		$category_table = ORM::getTable('model_categories');
		$out = array();

		$handle = DB::instance()->query("SELECT id FROM {$category_table} order by `order`");
		foreach($handle as $res) {
			$category = new model_categories($res["id"]);
			$category->is_parent = 0;
			$chandle = DB::instance()->query("SELECT count(id) as cnt FROM {$category_table} WHERE parent_id = ".$res['id']);
			$cres = $chandle->fetch();
			$category->parent_count = $cres['cnt'];
			$category->count = "0";
			$out[$category->id] = $category;
		}

		$handle = DB::instance()->query("SELECT category_id,count(category_id) as cnt FROM {$asset_table} GROUP BY category_id")->fetchAll();
		foreach($handle as $cnt) $out[$cnt['category_id']]->count = $cnt["cnt"];
		foreach($out as $category_id => $category) {
			if ($category->parent_id <> 0) {
				if (is_object($out[$category->parent_id])) {
					$out[ $category_id ]->parent_name = $out[ $category->parent_id]->long_name();
					$out[ $category_id ]->is_parent = 1;
				}
			}
		}
		return $out;

	}

	static function setOrder($order,$id=NULL) {
		$handle = DB::instance()->prepare("UPDATE categories SET `order` = :order WHERE id = :id");
		$handle->execute(array(
			':order'=>$order,
			':id'=>$id
		));
	}

	function selectedAssets($config,$recursive=false) { //Counts the # of selected assets under a category
		$asset_table = self::getTable('model_assets');
		$handle = DB::instance()->prepare("SELECT count(id) as cnt FROM {$asset_table} WHERE category_id = :category_id");
		$handle->execute(array(
			':category_id'=>$this->id
		));
		$res = $handle->fetch();
		return $res['cnt'];
	}

	static function buildTree($category_id,$root_id=0,$depth=0,$avatarType=1) {
		$node = new model_categories($category_id);
		$payload = array();
		$nodeChildren = $node->children();
		foreach($nodeChildren as $child) {
			$child = new model_categories($child->id);
			if (!is_null($child->avatarType)) {
				if ($avatarType != $child->avatarType) continue;
			}
			if (($node->id != $root_id)) {
				//if ($child->id != $root_id) continue; //Keep exclusive categories exclusive
				$payload[$child->id] = self::buildTree($child->id,$root_id,$depth++);
			} else {
				$payload[$child->id] = self::buildTree($child->id,$root_id,$depth++);
			}
		}
		foreach($node->allAssets(true) as $asset_id) $payload['assets'][$asset_id] = true;

		return $payload;
	}

	function findRoot() {
		//Find Root
		$sub = $this;

		while($sub->parent_id <> 0) {
			$last = $sub;
			$sub = new self($sub->parent_id);
		}

		return $last->id;
	}

	function upRoot($recur=FALSE,$out=array()) {
		if ($recur == false) {
			$root = $this->findRoot();
		} else {
			$root = $recur;
		}
		$table = self::getTable('model_assets');
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE category_id = :catid");
		$handle->execute(array(
			':catid'=>$root
		));
		while($res = $handle->fetch()) $out[] = $res['id'];

		$table = self::getTable('model_categories');
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE parent_id = :catid");
		$handle->execute(array(
			':catid'=>$root
		));
		while($res = $handle->fetch()) $out = $this->upRoot($res['id'],$out);
		return $out;
	}

	function randomAsset( $avatarType=NULL ) {
		$table = self::getTable('model_assets');
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE ((avatarType = :avatarType) or (avatarType IS NULL)) and category_id = :category_id ORDER BY RAND() LIMIT 1");
		$attr = array(
			':avatarType'=>$avatarType,
			':category_id'=>$this->id
		);
		$res = $handle->execute($attr);
		if ($res = $handle->fetch()) return $res['id'];
		return false;
	}

	function requiredAssets( $avatarType = NULL ) {
		$out = array();
		$table = self::getTable('model_assets');
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE (avatarType = :avatarType or ISNULL(avatarType)) and category_id = :category_id AND (flags & ".model_assets::REQUIRED.")");
		$handle->execute(array(
			':avatarType'=>$avatarType,
			':category_id'=>$this->id
		));
		if ($res = $handle->fetch()) $out[] = $res['id'];
		return $out;
	}

	function breadcrumb($catid,$build=NULL) { //Build breadcrumb trail array
		if (is_null($build)) $build = array();
		$cat = new self($catid);
		$build[] = array($cat->name,NULL);
		if ($cat->parent_id != 0) {
			$build = self::breadcrumb($cat->parent_id,$build);
		}
		return $build;
	}

	function admin_breadcrumb($catid,$build=NULL) { //Build breadcrumb trail array
		if (is_null($build)) $build = array();
		$cat = new self($catid);
		$build[] = array($cat->name,Router::instance()->generate('admin/assets',array('id'=>$catid)));
		if ($cat->parent_id != 0) {
			$build = self::admin_breadcrumb($cat->parent_id,$build);
		}
		return $build;
	}


	function children($exclusive_only=false) {
		if ($exclusive_only) {
			if ($exclusive_only == self::CHILDREN_REQUIRED_ONLY) {
				$handle = DB::instance()->prepare("SELECT id FROM categories WHERE parent_id = :parent_id and (flags & ".model_categories::ASSET_EXCLUSIVE." ) ORDER BY parent_id ASC,`order`");
			} elseif ($exclusive_only == self::CHILDREN_REQUIRED_FIRST) {
				$handle = DB::instance()->prepare("SELECT id, (flags & ".model_categories::REQUIRED." ) as sort_order FROM categories WHERE parent_id = :parent_id ORDER BY parent_id ASC,`order`");
			}
		} else {
			$handle = DB::instance()->prepare("SELECT id FROM categories WHERE parent_id = :parent_id ORDER by `order`");
		}
		$handle->execute(array(
			':parent_id'=>$this->id
		));
		$children = array();
		while($res = $handle->fetch()) {
			$children[$res['id']] = new self($res['id']);
		}
		return $children;
	}

	static function positioned($cat_id,$position) { //Get all categories in a specific position
		$out = array();
		$handle = DB::instance()->prepare("SELECT id FROM categories WHERE ((position_category_id=:pci) or (position_category_id=0 and parent_id=:pid)) and position=:position");
		$handle->execute(array(
			':pci'=>$cat_id,
			':pid'=>$cat_id,
			':position'=>$position
		));
		while($res = $handle->fetch()) {
			$out[] = new model_categories($res['id']);
		}
		return $out;
	}

	function asset_count() {
		$asset_table = self::getTable('model_assets');
		$handle = DB::instance()->prepare("SELECT count(id) as cnt FROM {$asset_table} WHERE category_id = :category_id");
		$handle->execute(array(
			':category_id'=>$this->id
		));
		$res = $handle->fetch();
		return $res['cnt'];
	}

	function allAssets($ids_only=FALSE) {
		$out = array();
		$asset_table = self::getTable('model_assets');
		$handle = DB::instance()->prepare("SELECT id FROM {$asset_table} WHERE category_id = :category_id");
		$handle->execute(array(
			':category_id'=>$this->id
		));
		if ($ids_only) {
			while($res = $handle->fetch()) $out[] = $res['id'];
		} else {
			while($res = $handle->fetch()) $out[$res['id']] = new model_assets($res['id']);
		}
		return $out;
	}

	function long_name($divider=' > ') {
		$current = $this;
		$name = array($this->name);
		while( $current->parent_id <> 0) {
			$current = new model_categories( $current->parent_id );
			$name[] = ( $current->name=='' ? 'Unnamed' : $current->name );
		}
		$name = array_reverse($name);
		return join($name,$divider);
	}

	//Iterate through categories establishing z-indexes
	static function layerMap($cat_id=NULL) {
		if(is_null($cat_id)) { //Start at the beginning
			$config = Config::instance();
			self::$layerMap = array();
			self::$zindex = 0;
			$cat_id = $config['engine']['starting_category_id'];
		}
		$category = new model_categories($cat_id); //Load current category

		if ( ! in_array("{$cat_id}-".model_rolls::POSITION_TOP,self::$layerMap)) { //Category yet mapped?
			foreach(model_categories::positioned($cat_id,model_rolls::POSITION_BOTTOM) as $pos_cat) model_categories::layerMap($pos_cat->id); //Iterate through categories attached to bottom
			self::$layerMap[++self::$zindex] = "{$cat_id}-".model_rolls::POSITION_BOTTOM; //Define this as the bottom of the category


			foreach(model_categories::positioned($cat_id,model_rolls::POSITION_BELOW) as $pos_cat) model_categories::layerMap($pos_cat->id); //Iterate through categories attached to below
			self::$layerMap[++self::$zindex] = "{$cat_id}-".model_rolls::POSITION_BELOW; //Define this as below the category


			foreach($category->children() as $child) { //Iterate through the children
				model_categories::layerMap($child->id); //Continue mapping
			}

			self::$layerMap[++self::$zindex] = "{$cat_id}-".model_rolls::POSITION_ABOVE;
			foreach(model_categories::positioned($cat_id,model_rolls::POSITION_ABOVE) as $pos_cat) model_categories::layerMap($pos_cat->id);

			self::$layerMap[++self::$zindex] = "{$cat_id}-".model_rolls::POSITION_TOP;
			foreach(model_categories::positioned($cat_id,model_rolls::POSITION_TOP) as $pos_cat) model_categories::layerMap($pos_cat->id);
		}
		return self::$layerMap;
	}

	static function fullName($cat_id,$carry='') {
		if ($cat = new model_categories($cat_id)) {
			if ($carry != '') {
				$buffer = $cat->name . "/" . $carry;
			} else {
				$buffer = $cat->name;
			}
			if ($cat->parent_id > 0) return self::fullName($cat->parent_id, $buffer);
			return $buffer;
		} else return 'Name not found';
	}

}
?>