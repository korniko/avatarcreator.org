<?php
class model_formats extends ORM {
	var $id;
	var $name;
	var $height;
	var $width;

	function __construct($id=NULL) {
		if (is_null($id)) return new self;
		parent::loadFromID($id);
	}

	static function all_formats() {
		$formats_table = ORM::getTable('model_formats');
		$out = array();
		$data = DB::instance()->query("SELECT id from {$formats_table}")->fetchAll();
		foreach($data as $format) {
			$out[$format['id']] = new model_formats($format['id']);
		}
		return $out;
	}

	static function fromType($type=NULL) {
		$c = Config::instance();
		$default = $c['engine']['format_default'];
		//TODO Build more logic into formats
		return new self($default);
	}

}
?>