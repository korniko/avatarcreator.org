<?php
	class model_identities extends ORM {
		var $user_id;

		//Animations
		var $hash_xs='';
		var $hash_sm='';
		var $hash_md='';
		var $hash_lg='';

		//Stills
		var $still_xs='';
		var $still_sm='';
		var $still_md='';
		var $still_lg='';

		var $rebuild=0; //Timestamp of when rebuild was initiated
		var $progress=0; //Progression

		var $asset_namespaces;

		var $avatar_type=1; // Male/Female
		var $config=array();
		var $root_crumb;
		var $format;

		CONST PROGRESS_NONE=0;
		CONST PROGRESS_STILL=1;
		CONST PROGRESS_ANIMATION=2;

		CONST TYPE_MALE=1;
		CONST TYPE_FEMALE=2;

		static function fromUser($user_id) {
			$user = new model_users($user_id);
			$table = self::getTable();
			$avatars = array();
			$handle = DB::instance()->prepare("SELECT id from {$table} WHERE user_id = :user_id");
			$handle->execute(array('user_id'=>$user_id));
			for($i=0;$i<$user->max_avatars;$i++) {
				if ($db_ident = $handle->fetch()) {
					$ident = new model_identities($db_ident['id']);
					$ident->format = $ident->format();
					array_shift($ident->root_crumb); //Remove display of starting point
					$identities[] = $ident;
				} else {
					$identities[] = NULL;
				}
			}
			return $identities;
		}

		function __construct($id=NULL) {
			$this->created = time();

			if (isset($this->user_id)) {
				//Sanity check progress/rebuilds
				$config_engine = Config::instance();
				if (($this->rebuild > 0) and (($this->rebuild - time()) < $config_engine['engine']['rebuild_delay'])) {
					//Rebuild in action
				} else { //Not rebuilding, set other vars accordingly
					$this->progress = self::PROGRESS_ANIMATION;
					$this->rebuild = 0;
					$this->save();
				}
			}


			if (! is_null($id)) parent::__construct($id);
			parent::__construct();
		}

		function format() {
			return model_formats::fromType ( $this->avatar_type );
		}

		function root($root_id) {
			$this->avatar_type=$root_id;
		}

		function setHash($hash,&$target) {
			if (file_exists(FKeeper::instance('avatars')->hashPath($target))) Fkeeper::instance('avatars')->delete( $target );
			return $target = $hash;
		}

		function randomAssets($parent_id=NULL,$avatar_type=1) { //Build avatar with bare minimum random assets based on category rules
			$c = Config::instance();
			//Start at root
			if (is_null($parent_id)) {
				$parent_id = $c['engine']['starting_category_id'];
				$this->config = array(); //Clear current config
			}
			$table = self::getTable('model_categories');
			$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE parent_id = :parent_id and ((avatarType = :avatarType) or (avatarType is NULL))");
			$handle->execute(array(
				':parent_id'=>$parent_id,
				':avatarType'=>$avatar_type
			));

			while($res = $handle->fetch()) {
				$category = new model_categories($res['id']);
				if (Flags::isFlag($category->flags,model_categories::REQUIRED)) { //Required -- Category
					//Choose an single asset at random
					if ($chosen_id = $category->randomAsset( $avatar_type )) { //Any Random Assets?
						//Does asset have options?
						$casset = new model_assets( $chosen_id );
						$rollOptions = $casset->rollOptions(TRUE); //Get Options
						if (count($rollOptions) > 0) { //If there are multiple options
							$default = $rollOptions[0]->id; //Choose the first
							$this->config_set($chosen_id, array('rids' => array($default)));
						} else {
							$this->config_set($chosen_id);
						}
						//Test for required assets
						foreach ($category->requiredAssets( $this->avatar_type ) as $rasset_id) { //Required -- Asset
							$this->config_set($rasset_id, NULL, TRUE);
						}
					}
				}
				$this->randomAssets($category->id,$avatar_type); //Recursive
			}
		}

		function buildAssetNamespace() {
			//Iterate through all assets building a namespace map
			$map = array();
			foreach($this->config as $asset_id=>$config) {
				$asset = new model_assets($asset_id);
				if ($asset->namespace <> '') $map[$asset->namespace] = $asset_id;
			}
			return $map;
		}

		function buildRollNamespace($asset_id) {
			//Iterate through all assets building a namespace map
			$map = array();
			$asset = new model_assets($asset_id);
			foreach($asset->loadRolls() as $roll_id=>$roll) {
				if ($roll->namespace <> '') $map[$roll->namespace] = $roll_id;
			}
			return $map;
		}

		function asset_namespace($namespace) { //Clears asset namespace
			if ($namespace <> '') { //We have an asset namespace
				//Remove all other assets in this namespace
				$map = $this->buildAssetNamespace();
				if (isset($map[$namespace])) unset($this->config[$map[$namespace]]);
			}
		}

		function roll_namespace($namespace,$asset_id) { //Clears asset namespace
			if ($namespace <> '') { //We have an asset namespace
				//Remove all other rolls in this namespace
				$map = $this->buildRollNamespace($asset_id);
				if (isset($map[$namespace])) unset($this->config[$asset_id]['rids'][$map[$namespace]]);
			}
		}

		//Config Controls
		function config_set($asset_id,$configs=NULL,$multi_override=false) {
			$asset = new model_assets($asset_id);
			if (is_null($configs)) { //Asset only
				$category = new model_categories($asset->category_id);
				if ($category->flags & model_categories::REQUIRED) { //Can toggle
					$this->asset_namespace($asset->namespace);
					$this->config[$asset_id] = $configs;
				} else { //OPTIONAL
					if ($this->config_isset($asset_id)) { //On -- Can not toggle off
						$this->asset_namespace($asset->namespace);
						unset($this->config[$asset_id]);
					} else { //Off, toggle to on
						$this->asset_namespace($asset->namespace);
						$this->config[$asset_id] = $configs;
					}
				}
			} else { //Configs (rolls) - adjust roll settings only
				if ($this->config_isset($asset_id)) { //Toggle Rolls
					$roll_ids = array_keys($configs);
					$roll_id = $roll_ids[0];
					if (isset($this->config[$asset_id]['rids'][$roll_id])) { //Toggle Off
						unset($this->config[$asset_id]['rids'][$roll_id]);
					} else { //Toggle On
						$roll = new model_rolls($roll_id);
						$this->roll_namespace($roll->namespace, $asset_id);
						$this->config[$asset_id]['rids'][$roll_id] = $configs[$roll_id];
					}
				} else { //Set Rolls
					$this->asset_namespace($asset->namespace);
					$this->config[$asset_id] = $configs;
				}
			}

		}

		function config_isset($asset_id) {
			return array_key_exists($asset_id,$this->config);
		}

		function config_unset($asset_id) {
			unset($this->config[$asset_id]);
		}

		function config_exclusion($category_id,$original=true) { //Remove all assets from config under this category id
			$cat = new model_categories($category_id);
			$removed = array();
			foreach ($cat->children() as $child) {
				$set = $this->config_exclusion($child->id,false);
				$removed = array_merge($removed,$set);
			}
			//Iterate through assets, unsetting each one.
			foreach ($cat->allAssets( TRUE ) as $asset_id) {
				if ($this->config_isset($asset_id)) {
					$this->config_unset($asset_id);
					//Record which one was removed to report to client.
					$removed[] = $asset_id;
				}
			}
			return $removed;
		}

		function buildResize(&$obj,$size) {
			//$obj->resizeImage($size,$size,Imagick::FILTER_LANCZOS,1);
			$obj->scaleImage($size,$size);
			//$obj->adaptiveResizeImage($size,$size,false);
		}

		function buildStep($add_config=NULL,$stillOnly=false) {
			//Build GIF via steps, use same as sprite animation - Use instead of build()
			set_time_limit(0);

			if (isset($this->user_id)) {
				$this->rebuild = time();
				$this->progress = self::PROGRESS_NONE;
				$this->save();
			}

			$config_engine = Config::instance();
			$start = time();
			if (!$this->config) $config = array($config_engine['engine']['missing_avatar']=>NULL); //Force Config
			//$this->config[$config_engine['engine']['default_bg']]=NULL; //Add default format background to config.
			$rolls = $this->rollsArray(); //Export rolls into an array
			ksort($rolls); //Sort Rolls
			$format = $this->format(); //Find Format
			$frame_map = array();
			foreach($rolls as $roll) {
				$roll_url = Router::instance()->generate('roll/view',array('id'=>$roll->id));
				$frame_map[$roll->id] = $roll->frame_count(); //Map Frames/Rolls
			}
			$tf = tempnam('/tmp','afb-');
			$flist = array();
			for($index_frame=1;$index_frame<=$config_engine['engine']['max_frames'];$index_frame++) {
				$index_name = sprintf("%02d",$index_frame);

				//Which frames to use?
				$current_frames = array();
				foreach($rolls as $roll) {
					//There is a problem with the next line. Sometimes frame_map [ roll_id ] is zero, which breaks the build
					$current_roll_frame_index = ceil( $index_frame / ($config_engine['engine']['max_frames'] / $frame_map[$roll->id]));
					if (($current_roll_frame_index > $config_engine['engine']['max_frames']) or ($current_roll_frame_index <= 0)) $current_roll_frame_index = 1;
					$current_frames[] = $roll->frame_pos($current_roll_frame_index);
				}

				//Create final frame holder
				$final = new Imagick();
				$final->newImage( $format->height, $format->width, new ImagickPixel('rgba(0,0,0,0)') );
				$final->setImageColorspace( Imagick::COLORSPACE_RGB );
				$final->setImageFormat('png');

				foreach($current_frames as $frame) { //Combine Frames
					$path = FKeeper::instance('frames')->hashPath( $frame->hash );
					$tmp = new Imagick( $path );
					$tmp->setImageColorspace( Imagick::COLORSPACE_RGB );
					$final->compositeImage($tmp, Imagick::COMPOSITE_DEFAULT, 0, 0);
					$tmp->destroy();
				}

				//Save frame to tmp file
				$fname = $tf."-{$index_name}-300.gif";
				$flist[300][] = $fname;
				$fp = fopen($fname,"wb");
				$final->setImageFormat('gif');
				fputs($fp,$final->getImageBlob());
				fclose($fp);

				/* Do other sizes for this frame */
				$st = clone $final;
				$st->setImageFormat('gif');
				$this->buildResize($st,150);
				//$st->thumbnailImage( 150, 150 );
				$fp = fopen("{$tf}-{$index_name}-150.gif","wb");
				fputs($fp,$st->getImageBlob());
				fclose($fp);
				$flist[150][]="{$tf}-{$index_name}-150.gif";

				$this->buildResize($st,75);
				//$st->thumbnailImage( 75, 75 );
				$fp = fopen("{$tf}-{$index_name}-75.gif","wb");
				fputs($fp,$st->getImageBlob());
				fclose($fp);
				$flist[75][]="{$tf}-{$index_name}-75.gif";

				$this->buildResize($st,32);
				//$st->thumbnailImage( 32, 32 );
				$fp = fopen("{$tf}-{$index_name}-32.gif","wb");
				fputs($fp,$st->getImageBlob());
				fclose($fp);
				$flist[32][]="{$tf}-{$index_name}-32.gif";

				if ($index_frame == 1) { //First image, use as still
					//Cache/Hash still
					$final->setImageFormat('jpg');
					$final->setCompressionQuality(100);
					foreach(array(300,150,75,32) as $size) {
						//$final->thumbnailImage($size,$size);
						$this->buildResize($final,$size);
						$fp = fopen("{$tf}-{$index_name}-{$size}.jpg","wb");
						fputs($fp,$final->getImageBlob());
						fclose($fp);
					}
					$this->setHash(FKeeper::instance('avatars')->keep( "{$tf}-{$index_name}-300.jpg" ),$this->still_lg);
					$this->setHash(FKeeper::instance('avatars')->keep( "{$tf}-{$index_name}-150.jpg" ),$this->still_md);
					$this->setHash(FKeeper::instance('avatars')->keep( "{$tf}-{$index_name}-75.jpg" ),$this->still_sm);
					$this->setHash(FKeeper::instance('avatars')->keep( "{$tf}-{$index_name}-32.jpg" ),$this->still_xs);
					if ($stillOnly === TRUE) {
						foreach( glob( "{$tf}-*") as $toDel ) {
							unlink($toDel);
						}
						return;
					}
					$this->progress = self::PROGRESS_STILL;
					$this->save();
				}
				unset($final);
			} //End Frame loop

			//Combine into GIFs

			//Compile tmp file and display results
			foreach(array(300,150,75,32) as $size) {
				$cmd = "/usr/bin/gifsicle -O1 -l0 -d10 --colors=256 ".join($flist[$size]," ")." > {$tf}-{$size}.gif";
				exec($cmd,$out);
			}

			$this->setHash(FKeeper::instance('avatars')->keep( "{$tf}-300.gif" ),$this->hash_lg);
			$this->setHash(FKeeper::instance('avatars')->keep( "{$tf}-150.gif" ),$this->hash_md);
			$this->setHash(FKeeper::instance('avatars')->keep( "{$tf}-75.gif" ),$this->hash_sm);
			$this->setHash(FKeeper::instance('avatars')->keep( "{$tf}-32.gif" ),$this->hash_xs);

			//Read files into a cache/hash
			$this->rebuild = 0;
			$this->progress = self::PROGRESS_ANIMATION;
			$this->save();

			//Clean tmp
			foreach( glob( "{$tf}-*") as $toDel ) {
				unlink($toDel);
			}
			return;
		}


		function build($add_config=NULL) {
			//Spawn proc to build avatars in bg
			/* Prevent Spammy requests
			if (($this->rebuild > 0) and ( (time() - $this->rebuild) < $config_engine['engine']['rebuild_delay'])) { //Already rebuilding
				return false;
			}
			*/
			$proc_command = "wget http://".URL_BASE."/doBuild/{$this->id} -q -O - -b";
			error_log('Building with : '.$proc_command,0);
			$proc = popen($proc_command, "r");
			pclose($proc);
		}

		// NOTE --- NO LONGER IN USE
		/*
		 *
		 * Builds an Avatar. Works as following
		 *  1 - Find pre existing renderings by SHA(1) on Config.
		 *  2 - If we need to build an avatar, then build the very first frame first, and launch an animated/extended build into the worker que.
		 *  3 - Worker que builds animations and thumbnails, then once done alerts the identity
		 *
		 * @param null $add_config
		 * @return bool
		 * @throws Exception
		function build($add_config=NULL) { //Sandwich assets, render results
			set_time_limit(0); //ToDo Speed this bitch up
			$config_engine = Config::instance();
			if ( ! $format = $this->format()) {
				$format = new model_formats($config_engine['engine']['format_default']);
			}

			$master = array();
			$gif = new GIFer(0, 2, array(-1, -1, -1), $format->width, $format->height);
			$config = (is_null($add_config) ? $this->config : array_merge($this->config,$add_config)); //Merge configs
			if (! $config) 	$config = array($config_engine['engine']['missing_avatar']=>NULL);
			$config[$config_engine['engine']['default_bg']]=NULL; //Add default format background to config.
			foreach($config as $asset_id=>$asset_config) { // Foreach Configuration
				$asset = new model_assets($asset_id);
				if (isset($asset_config['rids']) and (count($asset_config['rids']) > 0)) {
					$layers = $asset->layerFrames($asset_config['rids']); //Extract layer data from specific assets
				} else {
					$layers = $asset->layerFrames(); //Extract layer data from all individual assets
				}

				foreach($layers as $frame_index=>$layer) { //Down the rabbit hole we go!
					foreach($layer as $zindex=>$layer_data) {
						$master[$frame_index][$zindex] = $layer_data; //Mesh layers, retaining z-index
					}
				}
			}

			if (isset($this->hash_lg)) FKeeper::delete( $this->hash_lg );
			$this->hash_lg = '';
			if (count($master) == 0) return false;
			$master = $this->flowAnimate($master); //Copy assets into empty frames to flow as animation
			foreach($master as $frame=>$layer ) {
				if (isset($previous_layer)) {
					foreach($previous_layer as $k=>$v) {
						if (!isset($layer[$k])) $layer[$k] = $v; //Carry frames through animation
					}
				}
				krsort($layer);
				$base = new Imagick();
				$base->setBackgroundColor(new ImagickPixel('transparent'));
				$base->newImage( $format->width, $format->height, new ImagickPixel('transparent') );
				$base = $base->flattenimages();
				$layer = array_reverse($layer);
				foreach($layer as $hash) {
					$base->compositeImage( new Imagick( FKeeper::instance()->hashPath($hash) ), imagick::COMPOSITE_ATOP, 0, 0 );
				}
				$base->setImageFormat("png");
				$gif->addFrame( $base->getImageBlob() , 50, false);
				unset($base);
				$previous_layer = $layer;
			}
			$tmpfname = tempnam(sys_get_temp_dir(),TMP_NAME_PREFIX);
			$fp = fopen($tmpfname,"wb");
			fputs($fp,$gif->getAnimation());
			fclose($fp);
			$this->hash_lg = FKeeper::instance()->keep($tmpfname);
			if ( ! $this->user_id) {
				$this->purge(); //Remove temporary hashes
			} else {
				$this->save(); //Save stateful ident
			}

			return true;

		}
		*/

		//Build an array of rolls belonging to identity
		function rollsArray() {
			$rollLayers = array();
			$map = array_flip( model_categories::layerMap() );
			foreach($this->config as $asset_id=>$asset_config) {
				$asset = new model_assets($asset_id);
				if (isset($asset_config['rids']) and (count($asset_config['rids']) > 0)) { //Has custom roll chosen
					$subset = $asset->layerRolls($asset_config['rids']); //Extract layer data from specific assets
				} else { //Roll is general ( no options selected )
					$subset = $asset->layerRolls(); //Extract layer data from all individual assets
				}
				foreach($subset as $position=>$v) {
					//$index_target = $k;
					if (is_null($v->position_category_id)) {
						$position_final = $position;
					} elseif ($v->position_category_id == 0) {
						$as = new model_assets($v->asset_id);
						$position_final = "{$as->category_id}-{$v->position}";
					} else {
						$position_final = $v->position_category_id."-".$v->position;
					}
					$index_target = $map[$position_final];
					while(isset($rollLayers[$index_target])) $index_target++;
					$rollLayers[$index_target] = $v;
				}
			}
			ksort($rollLayers);
			return $rollLayers;
		}

		//Maps assets to flow as an animation
		function flowAnimate($map) {
			$next = NULL;
			foreach($map as $frame=>$set) {
				if ( ! is_null($next) ) {
					$new = $next;
					foreach($set as $k=>$v) {
						$new[$k] = $v;
					}
				} else {
					$new = $set;
				}
				$outmap[$frame] = $new;
				$next = $new;
			}
			return $outmap;
		}

		function purge() { //Remove all hashes of mine
			if (isset($this->hash_xs)) FKeeper::delete( $this->hash_xs );
			if (isset($this->hash_sm)) FKeeper::delete( $this->hash_sm );
			if (isset($this->hash_md)) FKeeper::delete( $this->hash_md );
			if (isset($this->hash_lg)) FKeeper::delete( $this->hash_lg );
		}

		//ORM Overrides
		function save() { //TODO Check if current user is authed to save avatars.
			$this->config = serialize($this->config);
			$ok = parent::save();
			$this->config = unserialize($this->config);
			return $ok;
		}

		function loadFromID($id=NULL) {
			parent::loadFromID($id);
			$this->config = unserialize($this->config);
			return $this;
		}

	}
?>
