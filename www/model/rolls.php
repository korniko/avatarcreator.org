<?php
	class model_rolls extends ORM {
		var $id;
		var $asset_id;
		var $flags=0;
		var $name;
		var $position_category_id; // Category to use when calculating global positioning
		var $position; // See position constants
		var $hash; //Rendered roll
		var $namespace='';
		var $spriteHash=''; //Rendered sprite

		var $frame_index=0;
		var $frame_count=0;

		CONST OPTIONAL=1;
		CONST SINGLE=2;

		const POSITION_ABOVE=1; //Default
		const POSITION_BELOW=2;
		const POSITION_TOP=3;
		const POSITION_BOTTOM=4;

		function __construct($id=NULL) {
			if (!is_null($id)) $this->loadFromID($id);
		}

		function loadFromID($id=NULL) {
			parent::loadFromID($id);
		}

		function save() {
			parent::save();
		}

		function build($format=NULL, $force_new = true) {
			if (isset($this->hash)) {
				if ($force_new) {
					FKeeper::delete($this->hash); //Remove pre-existing
				} else {
					return $this->hash;
				}
			}
			//Build Roll
			$config_engine = Config::instance();
			if (is_null($format)) {
				$format = new model_formats($config_engine['engine']['format_default']);
			}
			$frames = 0;
			//$gif = new gifCreator();
			$gif = new GIFer();
			//$gifFrame = $gifDelay = array();
			foreach($this->allFrames() as $frame) {
				//$base = new Imagick( FKeeper::instance()->hashPath($frame->hash) );
				$base = new Imagick();
				$base->newImage( $format->width, $format->height, new ImagickPixel('rgba(0,0,0,0)') );
				//$base->setImageBackgroundColor(new ImagickPixel('rgba(0,0,0,0)'));
				//$base->compositeImage( new Imagick( FKeeper::instance()->hashPath($frame->hash) ), imagick::COMPOSITE_ATOP, 0, 0 );
				$base->readImage( FKeeper::instance()->hashPath($frame->hash) );
				//$base->flattenImages();
				$base->setImageFormat("png");
				$gif->addFrame($base->getImageBlob(),50);
				//$gifFrame[] = $base->getImageBlob();
				//$gifDelay[] = 50;
				//unset($base);
				$frames++;
			}
			//$gif->create($gifFrame,$gifDelay);
			$tmpfname = tempnam(sys_get_temp_dir(),TMP_NAME_PREFIX);
			$fp = fopen($tmpfname,"wb");
			if ($frames > 0) {
				fputs($fp, $gif->getAnimation());
			} else {
				fputs($fp,$base->getImageBlob());
			}
			fclose($fp);
			$this->hash = FKeeper::instance()->keep($tmpfname);
			$this->save();
			return $this->hash;
		}

		function moveFrame($frame_id,$forward=TRUE) {
			$this->maxPosition(true); //Sanify position first
			$target = new model_frames($frame_id);
			if ($forward) {
				$replace = $target->position;
				$target->position += 1;
			} else {
				$replace = $target->position;
				$target->position -= 1;
			}
			$target->save();

			foreach($this->allFrames() as $position=>$frame) {
				if ($target->position == $frame->position) {
					$frame->position = $replace;
					$frame->save();
					return;
				}
			}
		}

		function frameFromFile($filename) { //Imports a file into a roll ( call ac_frameConvert before pls )
			$frame = new model_frames();
			$frame->roll_id = $this->id;
			$frame->position = $this->maxPosition();

			if ($frame->location = FKeeper::instance()->keep($filename)) {
				$frame->hash = FKeeper::instance()->hash;
				$frame->roll_id = $this->id;
				$frame->save();
			} else return false;
			return $frame;
		}

		function allFrames() {
			$frames = array();
			$frame_table = ORM::getTable('model_frames');
			$handle = DB::instance()->prepare("SELECT id FROM {$frame_table} WHERE roll_id = :roll_id ORDER BY id");
			$handle->execute(array(
				':roll_id'=>$this->id
			));
			while($res = $handle->fetch()) {
				$frames[] = new model_frames($res['id']);
			}
			return $frames;
		}

		function frame_count() {
			$frame_table = ORM::getTable('model_frames');
			$handle = DB::instance()->prepare("SELECT count(id) FROM {$frame_table} WHERE roll_id = :roll_id");
			$handle->execute(array(
				':roll_id'=>$this->id
			));
			$res = $handle->fetch();
			$this->frame_count = $res[0];
			return $res[0];
		}

		function delete() {
			foreach($this->allFrames() as $frame) {
				$frame->delete();
			}
			parent::delete();
		}

		function frame_pos($pos) {
			//Return frame # of roll
			$cur_pos = 1;
			foreach($this->allFrames() as $frame) {
				if ($cur_pos++ == $pos) return $frame;
			}
			return false;
		}

		function frame_next() {
			if ($this->frame_count == 0) $this->frame_count();
			if ($this->frame_count == 0) return false;
			if ($this->frame_index >= $this->frame_count) {
				$index = ($this->frame_index % $this->frame_count);
			} else {
				$index = $this->frame_index;
			}
			$frame_table = ORM::getTable('model_frames');
			$handle = DB::instance()->prepare("SELECT id FROM {$frame_table} WHERE roll_id = :roll_id LIMIT {$index},1");
			$handle->execute(array(
				':roll_id'=>$this->id
			));
			$this->frame_index++;
			if ($res = $handle->fetch()) {
				return new model_frames($res['id']);
			}
			return false;
		}

		function maxPosition($makeSane=FALSE) {
			$position = 0;
			$frame_table = ORM::getTable('model_frames');
			$handle = DB::instance()->prepare("SELECT id as frame_id FROM {$frame_table} WHERE roll_id = :roll_id ORDER by position");
			$handle->execute(array(
				':roll_id'=>$this->id
			));

			while ($res = $handle->fetch()) {
				$position++;
				if ($makeSane === TRUE) DB::instance()->exec("UPDATE {$frame_table} SET position = ".$position." WHERE id = ".$res['frame_id']);
			}
			return $position+1;
		}

		function buildSprite($format=NULL,$force_new=false) {
			//Build Sprite sheet for Roll
			if (($force_new == false) and ($this->spriteHash)) {
				return $this->spriteHash;
			}

			$config_engine = Config::instance();
			if (is_null($format)) {
				$format = new model_formats($config_engine['engine']['format_default']);
			}

			$sp = new Spritero($format->width,$format->height);
			foreach($this->allFrames() as $frame) $sp->addFrame( file_get_contents( FKeeper::instance('frames')->hashPath($frame->hash) ) );


			//$gif->create($gifFrame,$gifDelay);
			$tmpfname = tempnam(sys_get_temp_dir(),TMP_NAME_PREFIX);
			$fp = fopen($tmpfname,"wb");
			fputs($fp, $sp->getBlob() );
			fclose($fp);
			$this->spriteHash = FKeeper::instance('rolls')->keep($tmpfname);
			$this->save();
			return $this->spriteHash;
		}
	}
?>
