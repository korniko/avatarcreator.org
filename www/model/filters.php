<?php

/**
 * Class model_filters
 *
 *  Filters are applied to rolls. They can have static definitions or predefined input controllers
 *
 */
	class model_filters {
			var $mode;
			var $parameters;
			var $statics;

			CONST MODE_HUE=1;
			CONST MODE_COLORIZE=2;
			CONST MODE_MASK=3;


		function __construct($id=NULL) {
			if (!is_null($id)) $this->loadFromID($id);
		}

	}
?>