<?php
class model_users extends ORM {
	var $email;
	var $password;
	var $identities=0; //Max idents
	var $verification;
	var $salt;
	var $created;
	var $last_login=0;
	var $last_ip='';
	var $fbid;
	var $flags=0; // See below

	var $max_avatars = 3;

	//Flags
	const UNVERIFIED=0;
	const VERIFIED=1;
	const EDITOR=2;
	const ADMIN=4;

	//Auth states
	const LOGGED_IN = 1;
	const PASSWORD_WRONG = 2;
	const NOT_VERIFIED = 3;
	const NOT_ACTIVE = 4;
	const LOGIN_ERROR = 5;
	const NOT_FOUND = 6;

	static function instanceByEmail($email) {
		$table = static::getTable();
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE email=?");
		$handle->execute([ $email ]);
		if ($handle->rowCount() == 0) return false;
		$row = $handle->fetch();
		return static::instance($row['id']);
	}

	static function instanceFromSession() {
		if (isset($_SESSION['user_id'])) {
			return static::instance($_SESSION['user_id']);
		} else {
			return false;
		}
	}

	static function login($email,$password,$remember=false) {
		if ($user = self::instanceByEmail($email)) {
			$match = $user->matchPassword($password);
			if ($match === TRUE) {
				if (Flags::isFlag($user->flags,self::VERIFIED)) {
					$user->last_ip = $_SERVER['REMOTE_ADDR'];
					$user->last_login = time();
					$user->save();
					return static::LOGGED_IN;
				} else {
					return static::NOT_VERIFIED;
				}
			} else {
				return static::PASSWORD_WRONG;
			}
		} else {
			return static::NOT_FOUND;
		}
	}

	function __construct($id=NULL) {
		$this->created = time();
		$this->verification = $this->genRandomString();
		return parent::__construct($id);
	}

	function calcIdents() { //Count the number of identities
		$table = self::getTable('model_identities');
		$handle = DB::instance()->prepare("SELECT count(id) as cnt FROM {$table} WHERE user_id=:user_id");
		$handle->execute(array(
			':user_id'=>$this->id
		));
		$res = $handle->fetch();
		$this->identities = $res['cnt'];
		$this->save();
		return $this->identities;
	}

	protected function build() {
		$this->verification = $this->genRandomString();
	}

	public function setPassword($password) {
		$this->salt = $this->genRandomString();
		$password = $this->salt . $password;
		$this->password = $this->hashData($password);
		return true;
	}

	public function setEmail($email) {
		$table = static::getTable();
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE email = ?");
		$handle->execute([$email]);
		if ($handle->rowCount() > 0) {
			return false;
		} else {
			$this->email = $email;
			return true;
		}
	}

	public function setVerified($toggle=true) {
		if ($toggle){
			$this->flags = $this->flags | self::VERIFIED;
		} else {
			$this->flags = $this->flags & ( ! self::VERIFIED );
		}
		$this->verification = $this->genRandomString();
		return true;
	}

	public function isAdmin() {
		return Flags::isFlag($this->flags,self::ADMIN);
	}

	protected function matchPassword($in_password) {
		return ($this->hashData( $this->salt.$in_password ) == $this->password);
	}

	protected function genRandomString($length = 50) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$string = '';
		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, strlen($characters) -1)];
		}
		return $string;
	}

	protected function hashData($data) {
		return hash_hmac('sha512',$data, \SITEKEY );
	}

	public function setFBID($fbid) {
		$this->fbid = $fbid;
	}

	static function instanceByFBID($fbid) {
		$table = static::getTable();
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE fbid = ?");
		$handle->execute([$fbid]);
		if ($res = $handle->fetch()) {
			$user_id = $res['id'];
			return new self($user_id);
		} else return false;

	}

}
?>