<?php
	class model_assets extends ORM {
		var $name;
		var $category_id;
		var $used=0;
		var $layers;
		var $created;
		var $cost=0;
		var $location_hash;
		var $flags=0;
		var $namespace='';
		var $roll_namespaces;
		var $avatarType;//Asset Exclusive to type? NULL = NO

		CONST REQUIRED=1; //If anything in same category is included, this asset will become an auto-multiple(stacked w/ chosen asset)
		CONST HIDDEN=2; //Hide asset during selection
		CONST OPTIONAL=4; //Optional in Required categories

		static function rollCount($asset_id) {
			$roll_table = ORM::getTable('model_rolls');
			$handle= DB::instance()->prepare("SELECT count(id) from {$roll_table} WHERE asset_id = ?");
			$handle->execute( array($asset_id) );
			$res = $handle->fetch();
			return $res[0];
		}


		function save() {
			//Count layers on save
			$roll_table = ORM::getTable('model_rolls');
			$handle = DB::instance()->prepare("SELECT count(id) as cnt FROM {$roll_table} WHERE asset_id = ?");
			$handle->execute( array( $this->id ) );
			$res = $handle->fetch();
			$this->layers = $res['cnt'];
			parent::save();
		}


		//TODO - Force Single, Optional, and Plugins/Effects
		function loadRolls() {
			$out = "";
			$roll_table = ORM::getTable('model_rolls');
			$handle= DB::instance()->prepare("SELECT id from {$roll_table} WHERE asset_id = ? ");
			$handle->execute( array($this->id) );
			while ($res = $handle->fetch()) $out[ $res['id'] ] = new model_rolls( $res["id"] );
			return $out;
		}

		function hasLocation() {
			if ( is_null($this->location_hash)) return false;
			return true;
		}

		//Build master array of all locations of stacked components
		//TODO Animations
		// Proposed Logic --
		// a. Map all categories relative to eachother.
		// b.
		//
		function layerFrames($only_rolls=NULL) { //Array of roll ids to include. Leave null to include all
			$layerMap = array_flip(model_categories::layerMap()); //TODO Cache
			$frame_counts=array();
			$rollsA = $this->loadRolls(); //All rolls
			$default_category_id = $this->category_id;
			foreach($rollsA as $roll) { //For each roll
				$frame_counts[] = $roll->frame_count();
				if (!is_null($only_rolls)) { //Only include optional rolls if they are in only_rolls
					if (Flags::isFlag($roll->flags,model_rolls::OPTIONAL) or Flags::isFlag($roll->flags,model_rolls::SINGLE)) {
						if (in_array($roll->id, $only_rolls)) $rolls[] = $roll;
					} else $rolls[] = $roll;
				} else {
					if ( ! Flags::isFlag($roll->flags,model_rolls::OPTIONAL)) $rolls[] = $roll;
				}
			}
			$lcm = Multiples::lcm_arr($frame_counts);
			for($i=0;$i<$lcm;$i++) { //Frames
				$stack = array();
				foreach($rolls as $roll) { //Stacks
					$position = model_rolls::POSITION_ABOVE;
					$frame = $roll->frame_next();
					if ($roll->position > 0) $position = $roll->position;
					if ($roll->position_category_id == 0) {
						$position_key = $this->category_id."-".$position;
					} else {
						$position_key = $roll->position_category_id."-".$position;
					}

					$zindex = $layerMap[$position_key];
					while(isset($stack[$zindex])) $zindex++; //TODO doesn't seem right
					$stack[$zindex] = $frame->hash;
				}
				$map[] = $stack;
			}
			return $map;
		}


		function layerRolls($only_rolls=NULL) { //Array of roll ids to include. Leave null to include all
			$layerMap = array_flip(model_categories::layerMap()); //TODO Cache
			$frame_counts=array();
			$rolls = array();
			$map = array();
			$rollsA = $this->loadRolls(); //All rolls
			$default_category_id = $this->category_id;
			foreach($rollsA as $roll) { //For each roll
				if (!is_null($only_rolls)) { //Only include optional rolls if they are in only_rolls
					if (Flags::isFlag($roll->flags,model_rolls::OPTIONAL) or Flags::isFlag($roll->flags,model_rolls::SINGLE)) {
						if (in_array($roll->id, $only_rolls)) $rolls[] = $roll;
					} else $rolls[] = $roll;
				} else {
					if ( ! Flags::isFlag($roll->flags,model_rolls::OPTIONAL)) $rolls[] = $roll;
				}
			}

			foreach($rolls as $roll) {
				$position = model_rolls::POSITION_ABOVE;
				if ($roll->position > 0) $position = $roll->position;
				if (is_null($roll->position_category_id)) {
					$position_key = $this->category_id."-".$position;
				} else {
					$position_key = $roll->position_category_id."-".$position;
				}

				//$zindex = $layerMap[$position_key];
				$zindex = $position_key;
				while(isset($map[$zindex]))$zindex++;
				$map[$zindex] = $roll;
			}
			return $map;
			/*
				$stack = array();
				foreach($rolls as $roll) { //Stacks
					$position = model_rolls::POSITION_ABOVE;
					$frame = $roll->frame_next();
					if ($roll->position > 0) $position = $roll->position;
					if ($roll->position_category_id == 0) {
						$position_key = $this->category_id."-".$position;
					} else {
						$position_key = $roll->position_category_id."-".$position;
					}

					$zindex = $layerMap[$position_key];
					while(isset($stack[$zindex])) $zindex++; //TODO doesn't seem right
					$stack[$zindex] = $frame->hash;
				}
				$map[] = $stack;

			return $map;
			*/
		}

		function delete() {
			foreach($this->loadRolls as $roll) {
				$roll->delete();
			}
			parent::delete();
		}

		function rollOptions($singleOnly=false) { //Check if rolls having options
			$out = array();
			foreach($this->loadRolls() as $roll) {
				if ($singleOnly and ($roll->flags & model_rolls::SINGLE)) {
					$out[] = $roll;
				} elseif ($roll->flags & model_rolls::OPTIONAL) {
					$out[] = $roll;
				}
			}
			return $out;
		}

		function build() { //Build Asset? Assest shouldn't be built. They coordinate rolls into avatars
			//Remove previous
			if (isset($this->location_hash)) FKeeper::delete( $this->location_hash );

			$format = model_formats::fromType( $this->avatarType );
			$gif = new GIFer(0, 2, array(-1, -1, -1), $format->width, $format->height);

			$layers = $this->layerFrames(); //Map layers of frames

			if (count($layers) == 0) return false; //Renderable?

			foreach($layers as $layer ) {
				$base = new Imagick();
				$base->setBackgroundColor(new ImagickPixel('transparent'));
				$base->newImage( $format->width, $format->height, new ImagickPixel('transparent') );
				$base = $base->flattenimages();
				$layer = array_reverse($layer);
				foreach($layer as $hash) {
					$base->compositeImage( new Imagick( FKeeper::instance()->hashPath($hash) ), imagick::COMPOSITE_ATOP, 0, 0 );
				}
				$base->setImageFormat("png");
				$gif->addFrame( $base->getImageBlob() , 50, false);
				unset($base);
			}
			$tmpfname = tempnam(sys_get_temp_dir(),TMP_NAME_PREFIX);
			$fp = fopen($tmpfname,"wb"); //Write Binary to temp file
			fputs($fp,$gif->getAnimation()); //Output
			fclose($fp);
			$this->location_hash = FKeeper::instance()->keep($tmpfname); //Store rendered location
			$this->save();
			unlink($tmpfname); //Clean-up
			return true;
		}

		function viewed() { //TODO - create view stats for assets
		}

	}
?>