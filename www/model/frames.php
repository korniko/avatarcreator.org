<?php
	class model_frames extends ORM {
		var $roll_id;
		var $hash;
		var $position;
		var $url;

		function __construct( $id=NULL ) {
			if (!is_null($id)) {
				parent::loadFromID($id);
			}
			$this->url = Router::instance()->generate('admin/frame/view',array('hash'=>$this->hash));
		}

		function location() {
			return FKeeper::instance()->hashPath($this->hash); //Use FKeeper Conventions
		}

		function hue($color) {
			//Load file
			$source = new Imagick(FKeeper::instance()->hashPath($this->hash));
			$original = new Imagick(FKeeper::instance()->hashPath($this->hash));

			//Create color image
			$colorer = new Imagick();
			$background = new ImagickPixel();
			$background->setColor($color); //Colorers color
			$colorer->newImage( $source->getImageWidth(), $source->getImageHeight(), $background ); //Create colorer

			//Composite them color layer
			$source->compositeImage($colorer,imagick::COMPOSITE_HUE,0,0);
			$source->compositeImage($original,imagick::COMPOSITE_COPYOPACITY, 0, 0 );

			//Remove original image
			FKeeper::delete($this->hash);

			//Save new image
			$newFile = tempnam('/tmp','ac-');
			$source->writeImage($newFile);

			//Switch hash
			$this->hash = FKeeper::instance()->keep($newFile);

			//Save Frame
			$this->save();
		}


		function delete() { //Remove file and db record
			FKeeper::delete($this->hash);
			parent::delete();
		}

		function maxPosition() {
		}

	}
?>