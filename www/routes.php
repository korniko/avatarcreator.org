<?php
return array(
	'front'=>array('GET|POST','/','controllers_Static#display'),
	'about'=>array('GET|POST','/about','controllers_Static#display'),
	'tos'=>array('GET|POST','/tos','controllers_Static#display'),
	'privacy'=>array('GET|POST','/privacy','controllers_Static#display'),
	'gallery'=>array('GET|POST','/gallery','controllers_Gallery#main'),
	'contact'=>array('GET|POST','/contact','controllers_Contact#main'),
	'forums'=>array('GET|POST','/forums[**:tail]','controllers_Forums#main'),

	//Admin Section
	'admin'=>array('GET|POST','/admin','controllers_Admin#main'),
	'admin/logout'=>array('GET|POST','/admin/logout','controllers_Admin#logout'),
	'admin/map'=>array('GET|POST','/admin/map','controllers_Admin#categoryMap'),
	'admin/categories'=>array('GET|POST','/admin/categories','controllers_AdminCategories#categories'),
	'admin/categories/tags'=>array('GET|POST','/admin/categoryTags','controllers_AdminCategories#jsonTags'),
	'admin/categories/edit'=>array('GET|POST','/admin/category/edit','controllers_AdminCategories#ajaxEdit'),
	'admin/categories/edit/id'=>array('GET|POST','/admin/category/edit/[i:id]','controllers_AdminCategories#ajaxEdit'),
	'admin/category'=>array('GET|POST','/admin/category/[i:id]','controllers_AdminCategories#category'),
	'admin/assets'=>array('GET|POST','/admin/assets/[i:id]','controllers_AdminAssets#assets'),
	'admin/asset'=>array('GET|POST','/admin/asset/[i:id]','controllers_AdminAssets#asset'),
	'admin/assetorder'=>array('GET|POST','/admin/asset_order/[i:id]','controllers_AdminAssets#asset_order'),

	'admin/roll'=>array('GET|POST','/admin/roll/[i:id]','controllers_AdminAssets#asset_roll'),

	'admin/frame/view'=>array('GET|POST','/admin/frame/[h:hash].[:format]?','controllers_AdminView#frame_view'),

	'admin/view'=>array('GET|POST','/admin/view/[i:id]','controllers_AdminView#view'),
	'admin/build'=>array('GET|POST','/admin/build/[i:id]','controllers_AdminView#build'),

	'register'=>array('GET|POST','/register','controllers_Register#display'),
	'register_verify_sent'=>array('GET|POST','/verify-sent','controllers_Static#display'),
	'register_verify'=>array('GET|POST','/verify/[i:user_id]/[*:code]','controllers_Register#verify'),
	'register_reverify'=>array('GET','/reverify/[i:user_id]','controllers_Register#reverify'),
	'login'=>array('GET|POST','/login','controllers_Login#login'),
	'fblogin'=>array('GET|POST','/fblogin','controllers_Login#fblogin'),
	'logout'=>array('GET|POST','/logout','controllers_Login#logout'),
	'settings'=>array('GET|POST','/settings','controllers_Settings#main'),
	'lost_password'=>array('GET|POST','/lost','controllers_Login#lost'),

	//Avatars
	'avatars'=>array('GET|POST','/avatars','controllers_Avatars#main'),
	'avatars/create'=>array('GET|POST','/avatars/create','controllers_Avatars#create'),
	'avatars/view/id'=>array('GET|POST','/avatars/view/[i:id]','controllers_Avatars#view'), //Animated view
	'avatars/sview/id'=>array('GET|POST','/avatars/sview/[i:id]','controllers_Avatars#sview'), //Still view
	'avatars/create/id'=>array('GET|POST','/avatars/create/[i:id]','controllers_Avatars#create'),
	'avatars/create/id/asset'=>array('GET|POST','/avatars/create/[i:id]/[i:asset_id]','controllers_Avatars#create'),
	'avatars/build/id'=>array('GET|POST','/avatars/build/[i:id]','controllers_Avatars#build'),
	'avatars/id'=>array('GET|POST','/avatars/[i:id]','controllers_Avatars#edit'),
	'avatars/del/id'=>array('GET|POST','/avatars/del/[i:id]','controllers_Avatars#del'),

	'avatar/preview'=>array('GET|POST','/avatar/preview','controllers_Avatars#preview'),
	'avatar/preview/id'=>array('GET|POST','/avatar/preview/[i:id]','controllers_Avatars#preview'),
	'avatar/preview-cat/id'=>array('GET|POST','/avatar/preview-cat/[i:id]','controllers_Avatars#preview'),
	'avatar/preview-cat'=>array('GET|POST','/avatar/preview-cat','controllers_Avatars#preview'),

	'avatar/edit/id'=>array('GET|POST','/avatar/[i:id]','controllers_Avatars#edit'),
	'avatar/save/id'=>array('GET|POST','/avatar/save/[i:id]','controllers_Avatars#editSave'),
	'avatar/doBuild'=>array('GET|POST','/doBuild/[i:id]','controllers_Avatars#doBuild'),
	'avatar/check/id'=>array('GET|POST','/check/[i:id]','controllers_Avatars#ajaxCheck'),

	'asset/config/aid'=>array('GET|POST','/asset/config/[i:id]/[i:aid]','controllers_Avatars#ajaxConfig'),
	'asset/config'=>array('GET|POST','/asset/config/[i:id]','controllers_Avatars#ajaxConfig'), //Simple Ajax Avatar Load

	'roll/view'=>array('GET|POST','/roll/[i:id].png','controllers_Avatars#viewRoll'),//View a roll sprite sheet



	// Misc
	'test'=>array('GET|POST','/test','controllers_Test#test'),
	'hash'=>array('GET|POST','/hash/[*:hash].gif','controllers_Test#hash'),
	'roll'=>array('GET|POST','/roll/[i:id].gif','controllers_Test#roll')
);
?>
