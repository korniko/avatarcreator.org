<?php include('admin/header.php'); ?>
	<form method="post" action="<?= Router::instance()->generate('admin/categories')?>">
		<div class="panel panel-default">
			<div class="panel-heading">
				ALL Categories
			</div>
			<div class="panel-body">
				<table class="table table-striped table-rounded">
					<thead>
					<tr>
						<th> Name </th>
						<th> Parent </th>
						<th> Assets/Categories </th>
						<th> Flags </th>
					</tr>
					</thead>
					<tbody>
					<?php
						if (count($categories) > 0) {
							foreach($categories as $category_id=>$category) {
					?>
						<tr>
							<td><a href="<?=Router::instance()->generate('admin/assets',array('id'=>$category_id))?>"> <?= $category->name ?> </a></td>
							<td> <?= (($category->parent_id == 0) ? ' &mdash; ' : $category->parent_name ) ?> </td>
							<td><a href="<?=Router::instance()->generate('admin/assets',array('id'=>$category_id))?>"> <?= $category->count ?> Assets / <?=$category->parent_count ?> Categories </a></td>
							<td> <?
									if (Flags::isFlag($category->flags,model_categories::REQUIRED)) echo " <span class='label label-info'> REQUIRED </span> &nbsp;";
									foreach($config['engine']['avatar_types'] as $type_id=>$type_name) {
										if ($category->avatarType == $type_id) echo " <span class='label label-warning'> {$type_name} </span> &nbsp;";
									}
								?>
							</td>
						</tr>
					<?php
							}
						} else { ?>
							<tr><td colspan=6 align=center><i> No Categories </i></td></tr>
						<?php }
					?>
					</tbody>
				</table>
			</div>
		</div>
	</form>

	<form method="POST">
		<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="myModalLabel">Edit Category</h4>
					</div>
				<div class="modal-body" id="category-edit">

				</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>
	</form>

<script>
	function category_edit(num) {
		$.ajax({
			url:'<?=Router::instance()->generate('admin/categories/edit')?>/'+num,
			success:function(data,status) {
				$('#category-edit').html(data);
			},
			beforeSend:function() {
				$('#category-edit').html('Loading');
				$('#categoryModal').modal('show');
			}
		})
	}
</script>
<?php include('admin/category-add.php'); ?>
<?php include('admin/footer.php'); ?>