<?php include('admin/header.php'); ?>
<form method="post">

	<div class="panel panel-default">
		<div class="panel-heading">
			Category
			<? include("breadcrumb.php"); ?>
		</div>
		<div class="panel-body">

			<div class='row'>
				<div class="col-md-6">
					<div class="input-group">
						<label>
							Name :
							<input type="text" name="name" value="<?=$category->name?>">
							<input type="hidden" name="category_id" value="<?=$category->id?>">
						</label>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group">
						<label>
							Parent Category :
							<select name="parent_id">
								<option value="0"> - None - </option>
								<?php
								foreach($categories as $list_category) {
									$long_name = $list_category->long_name();
									echo "<option ";
									if ($category->parent_id == $list_category->id) echo " selected ";
									echo " value=\"{$list_category->id}\">{$list_category->long_name()}</option>\n";
								}
								?>
							</select>
						</label>
					</div>
				</div>
			</div>

			<div class='row'>
				<div class="col-md-6">
					<div class='btn-group' data-toggle='buttons'>
						<label class='btn btn-primary <?=($category->flags & model_categories::REQUIRED ? 'active':'')?>'>
							<input type='checkbox' name='flags[required]' value='1' <?=($category->flags & model_categories::REQUIRED ? 'checked':'')?>> 1 Required
						</label>
					</div>
					<div class='btn-group' data-toggle='buttons'>
						<label class='btn btn-primary <?=($category->avatarType == $type_id ? 'active':'')?>'>
							<input type='radio' name='avatarType' value='' <?=(is_null($category->avatarType) ? 'checked':'')?>> All
						</label>
						<?php foreach($config['engine']['avatar_types'] as $type_id=>$type_name) {?>
							<label class='btn btn-primary <?=($category->avatarType == $type_id ? 'active':'')?>'>
								<input type='radio' name='avatarType' value='<?=$type_id?>' <?=($category->avatarType == $type_id ? 'checked':'')?>> <?=$type_name?>
							</label>
						<?php } ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group">
						<label>
							Position :
							<select name="position">
								<option value="<?= model_rolls::POSITION_ABOVE ?>" <?=($category->position == model_rolls::POSITION_ABOVE ? 'selected':'')?>> Above </option>
								<option value="<?= model_rolls::POSITION_BELOW ?>" <?=($category->position == model_rolls::POSITION_BELOW ? 'selected':'')?>> Below </option>
								<option value="<?= model_rolls::POSITION_TOP ?>" <?=($category->position == model_rolls::POSITION_TOP ? 'selected':'')?>> Top </option>
								<option value="<?= model_rolls::POSITION_BOTTOM ?>" <?=($category->position == model_rolls::POSITION_BOTTOM ? 'selected':'')?>> Bottom </option>
							</select>

							<select name="position_category_id">
								<option value="0">- Parent Category -</option>
								<?php
								foreach($categories as $list_category) {
									$long_name = $list_category->long_name();
									echo "<option ";
									if ($category->position_category_id == $list_category->id) echo " selected ";
									echo " value=\"{$list_category->id}\">{$list_category->long_name()}</option>\n";
								}
								?>
							</select>
						</label>
					</div>
				</div>
			</div>
			<input type="submit" class="btn btn-large btn-success pull-right" name="cmd" value="Save">
		</div>
	</div>

	<div class="panel panel-success">
		<div class="panel-heading">
				Assets for <B><?= $category->name ?></B>
		</div>


		<div class="panel-body">

			<table class="table table-stripped">
				<thead>
					<tr>
						<td> Name </td>
						<td> Flags </td>
						<td> Rolls </td>
						<td> Used </td>
						<td> Action </td>
					</tr>
				</thead>
				<tbody>

				<?php
					if (count($assets) > 0) {
					foreach($assets as $asset_id=>$asset) { ?>
					<TR data-id="<?=$asset->id?>">
						<TD> <A HREF="<?= Router::instance()->generate("admin/asset",array("id"=>$asset->id))?>"><?= $asset->name ?></A> </TD>
						<TD> <?
							if (Flags::isFlag($asset->flags,model_assets::REQUIRED)) echo " <span class='label label-info'> REQUIRED </span> &nbsp;";
							if (Flags::isFlag($asset->flags,model_assets::HIDDEN)) echo " <span class='label label-default'> HIDDEN </span> &nbsp;";
							if (Flags::isFlag($asset->flags,model_assets::OPTIONAL)) echo " <span class='label label-warning'> <i class=\"fa fa-plus\"></i> OPTIONAL </span> &nbsp;";
							?>
						</TD>
						<TD> <?= $asset->roll_count ?> </TD>
						<TD> <?= $asset->used ?> </TD>
						<TD>
							<button class='btn btn-danger' onClick="return confirm('Remove Asset?');" name='delete' value='<?=$asset->id?>'>  <i class="fa fa-times-circle"></i> Delete  </button>
						</TD>
					</TR>
				<?php }
					} else { ?>
						<TR><TD colspan=5 align=center> <I> No Assets </I> </TD></TR>
				<?php } ?>

				</tbody>
			</table>

		</div>
	</div>
</form>



<form method="POST">
	<div class="panel panel-success">
		<div class="panel-heading">
			Create New Asset
		</div>
		<div class="panel-body">
			<div class="col-md-6">
				<div class="input-group">
					<label>
						Name :
						<input type="text" name="name" value="">
					</label>
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-group">
					<button name="cmd" value="add_category" class="btn btn-primary pull-right"> <i class="fa fa-plus"></i> Add Asset </button>
				</div>

			</div>
		</div>
	</div>

	<div class="panel panel-info">
		<div class="panel-heading">
			Sub Categories
		</div>
		<div class="panel-body">
			<table class="table table-striped" id="sortTable">
				<thead>
				<td> Name </td>
				<td> Flags </td>
				</thead>
				<tbody>
				<? foreach($children as $child) { ?>
					<tr class='catSorter' data-cat='<?=$child->id?>'>
						<td>
							<i class='fa handle fa-bars handle text-muted'></i>
							<a href="<?=Router::instance()->generate("admin/assets",array("id"=>$child->id))?>"><?=$child->name?></a>
						</td>
						<td><?
							if (Flags::isFlag($child->flags,model_categories::REQUIRED)) echo " <span class='label label-info'> REQUIRED </span> &nbsp;";
							foreach($config['engine']['avatar_types'] as $type_id=>$type_name) {
								if (Flags::isFlag($child->avatarType,$type_id)) echo " <span class='label label-warning'> {$type_name} </span> &nbsp;";
							}
							?></td>
					</tr>
				<? } ?>
				</tbody>
			</table>
		</div>
	</div>

</form>

<?php include('admin/category-add.php'); ?>


	<form method="POST">
		<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="myModalLabel">Edit Category</h4>
					</div>
					<div class="modal-body" id="category-edit">

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>
	</form>

	<script>
		function category_edit(num) {
			$.ajax({
				url:'<?=Router::instance()->generate('admin/categories/edit')?>/'+num,
				success:function(data,status) {
					$('#category-edit').html(data);
				},
				beforeSend:function() {
					$('#category-edit').html('Loading');
					$('#categoryModal').modal('show');
				}
			})
		}
		$(function  () {
			$('#sortTable tbody').sortable({
				handle: ".handle",
				update : function(e,u) {
					var orders='0';
					$('.catSorter').each(function(i){
						orders = orders + ',' + $(this).data('cat');
					});
					$.getJSON('<?=Router::instance()->generate("admin/assetorder",array('id'=>$category->id))?>', {
							returnFormat:'JSON',
							order:orders
						},
						function(data){
							if(data.success != 1){
								alert('Sorry, there was a problem updating sort orders.');
							}
					});
				}
			})
		});
	</script>



<?php include('admin/footer.php'); ?>