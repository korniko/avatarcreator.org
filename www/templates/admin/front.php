<?php include('admin/header.php'); ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			Dashboard
		</div>
		<div class="panel-body">
			<div class='col-md-6'>
				<table class='table table-striped'>
					<tr><td> Avatars : </td><td> <?=$stats['avatars']?> </td></tr>
					<tr><td> Categories : </td><td> <?=$stats['categories']?> </td></tr>
					<tr><td> Assets : </td><td> <?=$stats['assets']?> </td></tr>
					<tr><td> Formats : </td><td> <?=$stats['formats']?> </td></tr>
				</table>
			</div>
		</div>
	</div>
<?php include('admin/footer.php'); ?>