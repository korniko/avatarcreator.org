<? /* Requires $categories and $formats */ ?>
<form method="post" action="<?= Router::instance()->generate("admin/categories")?>">
	<div class="panel panel-info">
		<div class="panel-heading">
			New Category
		</div>
		<div class="panel-body">
			<div class='row'>
				<div class="col-md-4">
					<div class="input-group">
						<label>
							Name : <BR>
							<input type="text" name="name" value="">
						</label>
					</div>
				</div>
				<div class="col-md-3">
					<div class="input-group">
						<label>
							Format : <BR>
							<select name="format_id">
								<?php
								foreach($formats as $format_id=>$format) {
									echo "<option value=\"{$format_id}\">{$format->name} ({$format->height}x{$format->width})</option>\n";
								}
								?>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-5">
					<div class="input-group">
						<label>
							Parent : <BR>
							<select name="parent_id">
								<option value="0"> - None - </option>
								<?php
								foreach($categories as $list_category) {
									$long_name = $list_category->long_name();
									echo "<option ";
									if ($category->id == $list_category->id) echo " selected ";
									echo " value=\"{$list_category->id}\">{$list_category->long_name()}</option>\n";
								}
								?>
							</select>
						</label>
					</div>
				</div>
			</div>
			<div class='row'>
				<div class="col-md-12 text-center">
					<div class='btn-group' data-toggle='buttons'>
						<label class='btn btn-primary <?=($category->flags & model_categories::REQUIRED ? 'active':'')?>'>
							<input type='checkbox' name='flags[required]' value='1'> Required
						</label>
					</div>
					<div class='btn-group' data-toggle='buttons'>
						<label class='btn btn-primary active'>
							<input type='radio' name='avatarType' value='' checked> All
						</label>
						<?php foreach($config['engine']['avatar_types'] as $type_id=>$type_name) {?>
							<label class='btn btn-primary'>
								<input type='radio' name='avatarType' value='<?=$type_id?>'> <?=$type_name?>
							</label>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>

		<div class='panel-footer'>
			<div class="text-right">
				<button name="cmd" value="add_category" class="btn btn-primary"> <i class="fa fa-plus"></i> Add Category </button>
			</div>
		</div>
	</div>
</form>
