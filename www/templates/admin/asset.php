<?php include('admin/header.php'); ?>

<form method="post" action="<?= Router::instance()->generate("admin/asset",array("id"=>$asset_id))?>">
	<div class="panel panel-default">
		<div class="panel-heading">
			<? include('breadcrumb.php'); ?>
		</div>

		<div class="panel-body">

			<div class="col-md-6">

				<div class="well">

					<div class="input-group">
						<label> Name : <input type="text" name="aname" value="<?=$asset->name?>"> </label>
					</div>

					<div class="input-group">
						<label> Cost : <input type="number" name="cost" value="<?=$asset->cost?>"> </label>
					</div>

					<div class="input-group">
						<label> Format : <block><?= $format->name?></block></label>
					</div>

					<div class="input-group">
						<label> Namespace : <input type="text" name="namespace" value="<?=$asset->namespace?>"> </label>
					</div>

					<div class="input-group">
						<label>
							Parent Category :
							<select name="category_id">
								<option value="0"> - None - </option>
								<?php
								foreach($categories as $list_category) {
									$long_name = $list_category->long_name();
									echo "<option ";
									if ($asset->category_id == $list_category->id) echo " selected ";
									echo " value=\"{$list_category->id}\">{$list_category->long_name()}</option>\n";
								}
								?>
							</select>
						</label>
					</div>


					<div class="text-center">
						<div class='btn-group' data-toggle='buttons'>
							<label class='btn btn-primary <?=($asset->flags & model_assets::REQUIRED ? 'active':'')?>'>
								<input type='checkbox' name='flags[required]' value='1' <?=($asset->flags & model_assets::REQUIRED ? 'checked':'')?>> Required
							</label>
							<label class='btn btn-primary <?=($asset->flags & model_assets::HIDDEN ? 'active':'')?>'>
								<input type='checkbox' name='flags[hidden]' value='1' <?=($asset->flags & model_assets::HIDDEN ? 'checked':'')?>> Hidden
							</label>
							<label class='btn btn-primary <?=($asset->flags & model_assets::OPTIONAL ? 'active':'')?>'>
								<input type='checkbox' name='flags[optional]' value='1' <?=($asset->flags & model_assets::OPTIONAL ? 'checked':'')?>> Optional
							</label>
						</div>

						<div class='btn-group' data-toggle='buttons'>
						<?php foreach($config['engine']['avatar_types'] as $type_id=>$type_name) {
							if ($category->avatarType == $type_id) echo "* {$type_name}";
						} ?>
						</div>

					</div>

				</div>

				<button name="cmd" class="btn btn-success btn-lg btn-block" value="update_asset"> Save </button>

			</div>
			<div class="col-md-6">
				<div id="preview" style="width:100%" class="text-center">
					<img id="view" class="thumbnail" onLoad="$('#view_indicator').html('')" style="display:inline" src="<?= Router::instance()->generate('admin/view',array('id'=>$asset->id)) ?>" height="<?=$format->height?>" width="<?=$format->width?>">
					<BR>
					<button class='btn btn-default' onClick="buildView();return false;">Force Rebuild</button>
					<div id="view_indicator"></div>
				</div>
			</div>
		</div>
	</div>

	<?php
		$layer = 0;
		if (is_array($rolls))
		foreach($rolls as $roll_id=>$roll)  {
			$layer++;
	?>

	<div class="panel panel-default">
		<div class="panel-heading">
			<B>Roll #<?= $layer ?></B>
			<span class="pull-right">
				<button class="btn btn-sm" type="submit" value="<?=$roll->id?>" name="delete" onClick="return confirm('Delete layer <?=$layer?>?');" >
					&times;
				</button>
			</span>
		</div>
		<div class="panel-body">
			<iframe name="layer_<?=$layer?>" src="<?= Router::instance()->generate("admin/roll",array("id"=>$roll->id))?>" scroll="no" style="border:0px;width:100%;height:<?=$format->height+14?>px;overflow:hidden">
			</iframe>
		</div>
		<div class="panel-footer">

			<label>
				Name :
				<input type="text" name='name[<?=$roll->id?>]' value="<?=$roll->name;?>">
			</label>
			<BR>
			<label>
				Namespace :
				<input type="text" name="roll_namespace[<?=$roll->id?>]" value="<?=$roll->namespace?>">
			</label>
			<BR>
			<label>
				Global Position  :
				<select name="position[<?=$roll->id?>]">
					<option value="<?= model_rolls::POSITION_ABOVE ?>" <?=($roll->position == model_rolls::POSITION_ABOVE ? 'selected':'')?>> Above </option>
					<option value="<?= model_rolls::POSITION_BELOW ?>" <?=($roll->position == model_rolls::POSITION_BELOW ? 'selected':'')?>> Below </option>
					<option value="<?= model_rolls::POSITION_TOP ?>" <?=($roll->position == model_rolls::POSITION_TOP ? 'selected':'')?>> Top </option>
					<option value="<?= model_rolls::POSITION_BOTTOM ?>" <?=($roll->position == model_rolls::POSITION_BOTTOM ? 'selected':'')?>> Bottom </option>
				</select>
			</label>
			<label>
				<select name="pcid[<?=$roll->id?>]">
					<option value="0"> Current Category </option>
					<?
					foreach($categories as $list_category) {
						$long_name = $list_category->long_name();
						echo "<option ";
						if ($roll->position_category_id == $list_category->id) echo " selected ";
						echo " value=\"{$list_category->id}\">{$list_category->long_name()}</option>\n";
					}
					?>
				</select>
			</label>

			<BR>

			<label>
				Policy :

				<div class='btn-group' data-toggle='buttons'>
					<label class='btn btn-primary <?=($roll->flags & model_rolls::OPTIONAL ? 'active':'')?>'>
						<input type='checkbox' name='roll_flags[optional][<?=$roll->id?>]' value='<?=model_rolls::OPTIONAL?>' <?=($roll->flags & model_rolls::OPTIONAL ? 'checked':'')?>> Optional
					</label>
				</div>

			</label>

			<button class='btn btn-sm btn-success pull-right' name='zsave[<?=$roll->id?>]' id='zsave_<?=$roll->id?>'>
				Save
			</button>

			<HR> ToDo - Filters

		</div>

	</div>

	<?php } ?>

	<button class="btn btn-block btn-primary" name="cmd" value="add_roll"> <i class="fa fa-plus"></i> Add Roll </button>

	<BR><BR>

</form>
<script>
	function buildView() {
		$('#view_indicator').html('Rebuilding <i class="fa fa-spin fa-spinning"></i>');
		$("#view").attr("src", "<?= Router::instance()->generate('admin/build',array('id'=>$asset->id)) ?>?"+ new Date().getTime());
	}

</script>
<?php include('admin/footer.php'); ?>
