<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" type="text/css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/assets/spectrum/spectrum.js"></script>
<link rel="stylesheet" href="/assets/spectrum/spectrum.css" type="text/css">

<style>
.roll {
	position: absolute; left: 0px; right: 0px; bottom: 0px; top: 0px; white-space: nowrap; font-size: 0; line-height: 0; overflow-x: scroll;
	background:url(
	data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGklEQVQYlWNYvXr1f3TMgA0MBYWDzDkUKQQADgaWAYEvSUQAAAAASUVORK5CYII=
	) repeat;
}
.block {
	display: inline-block;
	height: 100%;
}

.block .over {
	visibility:hidden;
	position:relative;
	top:0px;
	left:0px;
}

.block:hover .over {
	visibility:visible;
}

.spaceless {
	position:fixed;
	top:0px;
}

.loader {
	visibility:hidden;
	background-color: #000;
	position:fixed;
	width:100%;
	height:100%;
	top:0px;
	left:0px;
	opacity:0.2;
	z-index:999;
	text-align:center;
}

.loader i {
	color:#fff;
	font-size:60px;
}

</style>
<form method="post" name="roll_form" id="roll_form" enctype="multipart/form-data">
<div style="display:none">
		<input type="file" name="file" id="upload" onChange="do_upload();submit();">
</div>

<div id="loader" class="loader"> <i class="fa fa-spin fa-spinner"></i> </div>
<div class="roll">
<? if (isset($frames) and count($frames) > 0) { ?>
	$first = 0;
	$last = false;
	<?php foreach($frames as $frame) {
		if ($first != count($frames)) {
			$first++;
		}
		if ($first == count($frames)) {
			$last = true;
		}
	?>

	<DIV class="block" style="width:<?=$format->width?>px;background-image:url(<?=$frame->url?>);background-size:cover">
		<div class="over" style="background-color: rgba(0, 0, 0, 0.6);height:100%;text-align:center;">
			<div style="width:80%;margin:0px auto;">
				<div style="padding-top:60px">
					<div class="btn-group text-center">
						<div class="btn btn-default btn-sm"> Colorize </div>
						<input type='text' name='color[<?=$frame->id?>]' class="btn btn-block color" />
					</div>
					<div class="clearfix"></div>
					<div class='btn-group btn-lg text-center'>
						<button class="btn btn-default <? if ($first == 1) echo 'disabled'?>" name="backward" value="<?=$frame->id?>"> <i class='fa fa-arrow-left'></i> Backward </button>
						<button class="btn btn-default <? if ($last) echo 'disabled'?>" name="forward" value="<?=$frame->id?>"> Forward  <i class='fa fa-arrow-right'></i> </button>
					</div>
					<button class="btn btn-info btn-block" name="duplicate" value="<?=$frame->id?>">  <i class='fa fa-copy'></i> Duplicate Frame </button>
					<button class="btn btn-danger btn-block" name="remove" value="<?=$frame->id?>"> <i class='fa fa-times'></i> Remove Frame</button>
				</div>
			</div>

		</div>
	</DIV>
	<?php } ?>
</div>

<div style="text-align:left;top:0;right:0;width:100%;position:fixed;">
	<button class="btn btn-sm btn-primary add-photo" onClick=" $('#upload').trigger('click');return false;"> <i class="fa fa-plus"></i> Add Frame(s)</button>
	<div class="label label-default"> <i class="fa fa-film"></i> <?= count($frames); ?> Frames </div>
</div>

</form>
<? } else { ?>
	<H1 style="text-align:center">No Frames in Roll</H1>
	<div style="top:0;right:0;width:100%;position:fixed;">
		<button class="btn btn-sm btn-primary add-photo" onClick=" $('#upload').trigger('click');return false;"> <i class="fa fa-plus"></i> Add Frame(s)</button>
		<div class="label label-default"> <i class="fa fa-film"></i> <?= count($frames); ?> Frames </div>
	</div>

<? } ?>
<script>
	$(document).ready(function() {
		$('.color').spectrum({
			preferredFormat: "hex",
			showInitial: true,
			showPalette: true,
			showSelectionPalette: true,
			palette: [ ],
			localStorageKey: "spectrum.homepage",
			change: function(color) {
				$('#roll_form').submit();
			}
		});
	});
	function do_upload() {
		$('#loader').css('visibility','visible');
	}
</script>