<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Danjelo Morgaux">

	<title>Avatar Foundry .com</title>

	<link rel="icon" href="/images/icon.png">
	<link rel="apple-touch-icon" href="/images/button.png">

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.min.css" type="text/css">
	<link rel="stylesheet" href="/assets/css/style.css" type="text/css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" type="text/css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="/assets/js/jquery-ui.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<script src="/assets/js/script.js"></script>
	<script src="/assets/js/bootstrap-tags.js"></script>
</head>

<body>

<div id="wrapper">

	<!-- Sidebar -->
	<nav class="navbar navbar-default" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/" style="text-align:center"> Avatar Foundry <BR> Admin</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav side-nav">
				<li><a href="/"><i class="fa fa-file"></i> Website </a></li>
				<li><a href="<?= Router::instance()->generate('admin')?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li><a href="<?= Router::instance()->generate('admin/categories')?>"><i class="fa fa-file"></i> Categories</a></li>
				<li><a href="<?= Router::instance()->generate('admin/map')?>"><i class="fa fa-map-marker"></i> Z-Map</a></li>
				<? /*
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Assets <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<?php
						foreach($categories as $k=>$v) {
							echo "<li><a href=\"".Router::instance()->generate("admin/assets",array("id"=>$k))."\">".$v->name."</a></li>\n";
						}
						?>
					</ul>
				</li>
                */ ?>
			</ul>

			<ul class="nav navbar-nav navbar-right navbar-user">
				<li class="dropdown user-dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= (isset($_SESSION["user"]) ? $_SESSION['user'] : 'Guest' )?> <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li class="divider"></li>
						<li><a href="<?=Router::instance()->generate('admin/logout');?>"><i class="fa fa-power-off"></i> Log Out</a></li>
					</ul>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</nav>
	<div id="page-wrapper" class="container">
