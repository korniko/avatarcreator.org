
				<div class='row'>
					<div class="col-md-6">
						<div class="input-group">
							<label>
								Name :
								<input type="text" name="name" value="<?=$category->name?>">
								<input type="hidden" name="category_id" value="<?=$category->id?>">
							</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="input-group">
							<label>
								Format :
								<select name="format_id">
									<?php
									foreach($formats as $format_id=>$format) {
										echo "<option value=\"{$format_id}\">{$format->name} ({$format->height}x{$format->width})</option>\n";
									}
									?>
								</select>
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="input-group">
							<label>
								Parent Category :
								<select name="parent_id">
									<option value="0"> - None - </option>
									<?php
									foreach($categories as $list_category) {
										$long_name = $list_category->long_name();
										echo "<option ";
										if ($category->parent_id == $list_category->id) echo " selected ";
										echo " value=\"{$list_category->id}\">{$list_category->long_name()}</option>\n";
									}
									?>
								</select>
							</label>
						</div>
					</div>

					<div class="col-md-12">
						<div class="input-group">
							<label>
								Position :
								<select name="position">
									<option value="<?= model_rolls::POSITION_ABOVE ?>" <?=($category->position == model_rolls::POSITION_ABOVE ? 'selected':'')?>> Above </option>
									<option value="<?= model_rolls::POSITION_BELOW ?>" <?=($category->position == model_rolls::POSITION_BELOW ? 'selected':'')?>> Below </option>
									<option value="<?= model_rolls::POSITION_TOP ?>" <?=($category->position == model_rolls::POSITION_TOP ? 'selected':'')?>> Top </option>
									<option value="<?= model_rolls::POSITION_BOTTOM ?>" <?=($category->position == model_rolls::POSITION_BOTTOM ? 'selected':'')?>> Bottom </option>
								</select>

								<select name="position_category_id">
									<option value="0">- Parent Category -</option>
									<?php
									foreach($categories as $list_category) {
										$long_name = $list_category->long_name();
										echo "<option ";
										if ($category->position_category_id == $list_category->id) echo " selected ";
										echo " value=\"{$list_category->id}\">{$list_category->long_name()}</option>\n";
									}
									?>
								</select>
							</label>
						</div>
					</div>
				</div>

				<div class='row'>
					<div class="col-md-12 text-center">
						<div class='btn-group' data-toggle='buttons'>
							<label class='btn btn-primary <?=($category->flags & model_categories::REQUIRED ? 'active':'')?>'>
								<input type='checkbox' name='flags[required]' value='1' <?=($category->flags & model_categories::REQUIRED ? 'checked':'')?>> Required
							</label>
						</div>
						<div class='btn-group' data-toggle='buttons'>
							<label class='btn btn-primary <?=($category->avatarType == $type_id ? 'active':'')?>'>
								<input type='radio' name='avatarType' value='' <?=(is_null($category->avatarType) ? 'checked':'')?>> All
							</label>
							<?php foreach($config['engine']['avatar_types'] as $type_id=>$type_name) {?>
							<label class='btn btn-primary <?=($category->avatarType == $type_id ? 'active':'')?>'>
								<input type='radio' name='avatarType' value='<?=$type_id?>' <?=($category->avatarType == $type_id ? 'checked':'')?>> <?=$type_name?>
							</label>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>