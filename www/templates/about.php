<?php 
$meta['title'] = "About ".SITE_NAME;
include("header.php");
?>
	<div class="container">
		<div class="block-text">
			<H1>About <?=SITE_NAME?></H1>

			<P><?=SITE_NAME?> was created in the summer of 2014. The creators and founders of <?=SITE_NAME?> are avid forum users and
				embrace the concept of the avatar.</P>

			<P>The original avatar creation engine was adapted from a sister site. The engine was then created into a web site and a basic unique graphic set was introduced.</P>

			<P>Talented artists were hired to develop the basic avatar set. Then extra artists were commissioned to add asset packs.</P>

			<P>We had fun creating this website, so we hope that you have fun using it.</P>

			<P>&nbsp;</P>
		</div>
		<BR><BR>
	</div>
<?
include("footer.php");
?>