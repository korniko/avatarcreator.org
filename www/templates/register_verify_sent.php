<?php
	include("header.php");
?>
	<div class='container'>
		<div class="block-text">
			<H1>Thank you for registering</H1>
			<P> An email has been sent to the email address you specified.</P>
			<P>Please check your email and follow the directions to activate your account. </P>
			<P> Thank you </P>
		</div>
		<BR>
	</div>
	<BR><BR>
<?php
	include("footer.php");
?>