</div>

<div class="footer text-center">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="nav nav-pills nav-justified">
					<li style="color:white">&copy; <?= date('Y'); ?> <?=SITE_NAME?> </li>
					<li><a href="/tos">Terms of Service</a></li>
					<li><a href="/privacy">Privacy</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<script>
jQuery(document).ready(function() {
	jQuery("abbr.timeago").timeago();
});
</script>
</body>
</html>
