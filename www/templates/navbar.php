<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/" title="<?=SITE_NAME?>"><img src="/assets/logo1.png" style="width:100%;max-width:54px;"></a>
		</div>
		<div class="navbar-collapse collapse">
			<? if (isset($me)) { // LOGGED IN ?>
			<ul class="nav navbar-nav">
				<li <?= ($route=='front'?"class='active'":"")?>><a href="/">Home</a></li>
				<li <?= ($route=='avatars'?"class='active'":"")?>><a href="<?=Router::instance()->generate('avatars');?>"> Avatars</a></li>

				<li <?= ($route=='gallery'?"class='active'":"")?>><a href="<?= Router::instance()->generate("gallery");?>">Gallery</a></li>
				<li <?= ($route=='about'?"class='active'":"")?>><a href="<?= Router::instance()->generate("about");?>">About</a></li>
				<li <?= ($route=='contact'?"class='active'":"")?>><a href="/contact">Contact</a></li>
				<? /*
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"> Identities <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li style="text-align:center"><a href="#"> <img src="http://www.placehold.it/50x50"></a></li>
					</ul>
				</li>
                */ ?>
				<? if ($me->isAdmin()) { ?>
				<li><a href="<?=Router::instance()->generate('admin');?>"> Admin</a></li>
				<? } ?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <?= $me->email ?> <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<? /*<li style="text-align:center"><a href="/profile"> My Profile </a></li>
						<li style="text-align:center"><a href="/settings"> Settings </a></li>
                        */ ?>
						<li style="text-align:center"><a href="/logout"> Logout </a></li>
					</ul>
				</li>
			</ul>
			<? } else { //NOT LOGGED IN ?>
				<ul class="nav navbar-nav">
					<li <?= ($route=='front'?"class='active'":"")?>><a href="/">Home</a></li>

					<li <?= ($route=='gallery'?"class='active'":"")?>><a href="<?= Router::instance()->generate("gallery");?>">Gallery</a></li>
					<li <?= ($route=='about'?"class='active'":"")?>><a href="<?= Router::instance()->generate("about");?>">About</a></li>
					<li <?= ($route=='contact'?"class='active'":"")?>><a href="/contact">Contact</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?= Router::instance()->generate("login")?>">Login</a></li>
				</ul>
			<? } ?>
		</div><!--/.nav-collapse -->
	</div>
</div>
