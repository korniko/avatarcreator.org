<?php require_once("header.php"); ?>

	<div class="carousel-reviews">
		<div class="container">
			<div class="row">
				<div id="carousel-reviews" class="carousel slide broun-light" data-ride="carousel" data-interval="10000" style="padding-bottom:20px">
					<div class="carousel-inner">
						<div class="item active">
							<div class="col-md-4 col-sm-6">
								<div class="block-text front-fixed rel zmax">
									<h1>Avatars</h1>
									<p>Let us manage your avatars. The <?=SITE_NAME?> can create and host avatars.</p>
									<ins class="ab zmax sprite sprite-i-triangle block"></ins>
								</div>
								<div class="person-text rel">
									<img src="/assets/preview/manager.png" width="200">
								</div>
							</div>
							<div class="col-md-4 col-sm-6 hidden-xs">
								<div class="block-text front-fixed rel zmax">
									<h1>Backgrounds</h1>
									<p>Choose through an assortment of animated, still, or regional backgrounds.</p>
									<ins class="ab zmax sprite sprite-i-triangle block"></ins>
								</div>
								<div class="person-text rel">
									<img src="/assets/preview/rocker.gif" width="200">
								</div>
							</div>
							<div class="col-md-4 col-sm-6 hidden-sm hidden-xs">
								<div class="block-text front-fixed rel zmax">
									<h1>Animated</h1>
									<p>Combine everything into a complex animated avatar</p>
									<ins class="ab zmax sprite sprite-i-triangle block"></ins>
								</div>
								<div class="person-text rel">
									<img src="/assets/preview/ann-done.gif" width="200">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="col-md-4 col-sm-6">
								<div class="block-text front-fixed rel zmax">
									<h1>Accessories & Clothes</h1> 
									<p>Customize your own outfits, colors, and costumes. </p>
									<ins class="ab zmax sprite sprite-i-triangle block"></ins>
								</div>
								<div class="person-text rel">
									<img src="/assets/preview/pirate.jpg" width="200">
								</div>
							</div>
							<div class="col-md-4 col-sm-6 hidden-xs">
								<div class="block-text front-fixed rel zmax">
									<h1>Make Up & Jewelry</h1>
									<p>Customize your own colors, shades, gemstones, and crystals </p>
									<ins class="ab zmax sprite sprite-i-triangle block"></ins>
								</div>
								<div class="person-text rel">
									<img src="/assets/preview/slider-girl-1.png" width="200">
								</div>
							</div>
							<div class="col-md-4 col-sm-6 hidden-sm hidden-xs">
								<div class="block-text front-fixed rel zmax">
									<h1>Expressive</h1>
									<p>Out of this world colors and add-ons.</p>
									<ins class="ab zmax sprite sprite-i-triangle block"></ins>
								</div>
								<div class="person-text rel">
									<img src="/assets/preview/5-misc.png" width="200">
								</div>
							</div>
						</div>
					</div>
					<a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
						<span class="fa fa-chevron-left"></span>
					</a>
					<a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
						<span class="fa fa-chevron-right"></span>
					</a>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row well">
				<div class="col-md-12 text-center">
					<H1> Build and Customize Animated Avatars </H1>
				</div>
				<? if (!isset($me)) { ?>
				<div class="col-md-6 text-center">
						<a href="<?= Router::instance()->generate('avatars')?>" class="btn btn-primary btn-block btn-lg"> My Avatars </a>
				</div>
				<div class="col-md-6 text-center">
					<a href="<?= Router::instance()->generate('register')?>" class="btn btn-success btn-block btn-lg"> Register </a>
				</div>
				<? } else { ?>
					<div class="col-md-12 text-center">
						<a href="<?= Router::instance()->generate('avatars')?>" class="btn btn-primary btn-block btn-lg"> My Avatars </a>
					</div>
				<? } ?>
			</div>
		</div>
	</div>
<?php require_once('footer.php'); ?>
