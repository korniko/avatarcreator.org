<?php
include("header.php");
?>
	<div class='container'>
		<BR>
		<form method="POST" class="form-horizontal">
			<fieldset>
				<legend>Login</legend>
				<div class='col-md-8'>
					<? if (isset($message)) { ?>
						<div class="alert alert-warning" role="alert"><?=$message?></div>
					<? } ?>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="textinput">Email Address</label>
						<div class="col-md-6">
							<input id="textinput" name="email" placeholder="Your Email Address" class="form-control input-md" type="text">
							<span class="help-block">Your E-mail address</span>
						</div>
					</div>

					<!-- Password input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="passwordinput">Password</label>
						<div class="col-md-6">
							<input id="passwordinput" name="password" placeholder="Password" class="form-control input-md" type="password">
							<span class="help-block"> Password to protect your account</span>
						</div>
					</div>

					<button class='btn btn-success btn-block'> Login </button>
					<BR>
				</div>
				<div class='col-md-4 text-center'>

					<p><a href="#" onClick="logInWithFacebook()"><img src="/assets/images/loginwfb.png"/></a></p>


					<BR>
					<a class='btn btn-primary' href="<?=Router::instance()->generate('register')?>"> Register </a>
					<BR><BR>
					<a class='btn btn-default' href="<?=Router::instance()->generate('lost_password')?>"> Lost Password ? </a>
				</div>

			</fieldset>
		</form>
		<BR>
	</div>
	<BR>
<?php
include("footer.php");
?>
