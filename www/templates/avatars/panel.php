<style>
	.avatar.empty {
		border:2px dashed #ddd;
		width:100%;
		height:100px;
		text-align:center;
		line-height:100px;
		font-size:20px;
		background:rgba(255,255,255,0.4);
	}
</style>
<?php
	if ($avatar == NULL) {
?>
		<A HREF="<?= Router::instance()->generate('avatars/create');?>">
			<div class="avatar empty">
				<P> <i class='fa fa-plus'></i> Create New Avatar</P>
			</div>
		</A><BR>
<?php
	} else {
?>
		<div class='panel panel-default'>
			<div class='panel-body'>
				<div class="row">
					<div class='col-md-4'>
						<div style="position:relative;height:<?=$avatar->format->height?>px;width:<?=$avatar->format->width?>px;margin:0 auto;">
							<img id="img<?=$avatar->id?>" src="<?
								if ($avatar->progress == model_identities::PROGRESS_STILL) { //Stills only ?>
									<?=Router::instance()->generate('avatars/sview/id',array('id'=>$avatar->id))?>
								<? } else { ?>
									<?=Router::instance()->generate('avatars/view/id',array('id'=>$avatar->id))?>
								<? }
							?>">

							<?php
							if ($avatar->rebuild > 0) {
								echo "<div id=\"picAlert{$avatar->id}\" style=\"position:absolute;width:100%;font-size:20px;color:white;bottom:0px;text-align:center;background-color:rgba(0,0,0,0.3)\">";
								echo "<i class=\"fa fa-spin fa-spinner\"></i> Rebuilding ... ";
								echo "</div>";
							}
							?>
						</div>
					</div>
					<div class="col-md-8">
						<div class="row" style="height:264px">
							<div class='col-sm-12'>
								<H2>Links to Avatar Images</H2>
								<div class='pull-right'>
									<label><i class='fa fa-info-circle'></i> JSON Config File : <input type='text' style='width:200px' disabled value='http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>.json'></label>
								</div>
								<table class='table table-striped' style="width:100%">
									<TR>
										<TH> Name </TH>
										<TH> Dimensions </TH>
										<TH> Animated GIF </TH>
										<TH> Size </TH>
										<TH> Still Jpeg </TH>
										<TH> Size </TH>
									</TR>
									<tr>
										<td>Large </td>
										<td> 300x300</td>
										<td> <A target="_BLANK" HREF="http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>l.gif">http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>l.gif</A> </td>
										<td> <?=$avatar->info['hash_lg']['sizeHuman']?> </td>
										<td> <A target="_BLANK" HREF="http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>l.jpg">http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>l.jpg</A> </td>
										<td> <?=$avatar->info['still_lg']['sizeHuman']?> </td>
									</tr>
									<tr>
										<td>Medium </td>
										<td> 150x150 </td>
										<td> <A target="_BLANK" HREF="http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>m.gif">http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>m.gif</A></td>
										<td> <?=$avatar->info['hash_md']['sizeHuman']?> </td>
										<td> <A target="_BLANK" HREF="http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>m.jpg">http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>m.jpg</A></td>
										<td> <?=$avatar->info['still_md']['sizeHuman']?> </td>

									</tr>
									<tr>
										<td>Small </td>
										<td> 75x75 </td>
										<td> <A target="_BLANK" HREF="http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>s.gif">http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>s.gif</A> </td>
										<td> <?=$avatar->info['hash_sm']['sizeHuman']?> </td>
										<td> <A target="_BLANK" HREF="http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>s.jpg">http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>s.jpg</A> </td>
										<td> <?=$avatar->info['still_sm']['sizeHuman']?> </td>
									</tr>
									<tr>
										<td>Tiny </td>
										<td> 32x32 </td>
										<td> <A target="_BLANK" HREF="http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>x.gif">http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>x.gif</A> </td>
										<td> <?=$avatar->info['hash_xs']['sizeHuman']?> </td>
										<td> <A target="_BLANK" HREF="http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>x.jpg">http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>x.jpg</A> </td>
										<td> <?=$avatar->info['still_xs']['sizeHuman']?> </td>
									</tr>
									<tr><td colspan=3 align=center>
											<a href="http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>f.gif" class="btn btn-sm btn-default"> <i class='fa fa-file-video-o'></i> Download Animated</a>
										</td>
										<td colspan=3 align=center>
											<a href="http://i.<?=URL_BASE?>/<?=base58::encode($avatar->id)?>f.jpg" class="btn btn-sm btn-default"> <i class='fa fa-file-picture-o'></i> Download Still</a>
										</td>
								</table>
								<div class="row">
									<div class="pull-right">
										<a href="<?= Router::instance()->generate('avatar/edit/id',array('id'=>$avatar->id));?>" class='btn btn-success btn-lg'> Edit </a>
										<a href="<?= Router::instance()->generate('avatars/del/id',array('id'=>$avatar->id));?>" onClick="return confirm('Are you sure?');" class='btn btn-warning btn-lg'> Clear </a>
										&nbsp;
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			setInterval(function() {
				$.ajax({
					url:"http://www.<?=URL_BASE?>/check/<?=$avatar->id?>",
					dataType:'json'
				}).done(function(data) {
					console.log('aid: <?=$avatar->id?>');
					console.log(data);
					if (data.stage == false) {
						$('#picAlert<?=$avatar->id?>').hide();
					} else {
						$('#picAlert<?=$avatar->id?>').show();
					}
					if (data.url != $('img<?=$avatar->id?>').attr('src')) {
						var tn = new Date;
						var newUrl = data.url+'?cb='+tn.getTime();
						$('img<?=$avatar->id?>').attr('src', newUrl.trim() );
					}
				});
			},10000)
		</script>
		<BR>
<?php
	}
?>
