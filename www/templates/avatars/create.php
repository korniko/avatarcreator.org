<?
	include("header.php");
?>
	<div class='container avatar-create'>

		<div class="col-md-12">
			<H2 id="category_title">
				Start a New Avatar
			</H2>
			<HR>
		</div>

		<? foreach($avatarTypes as $typeValue=>$typeName) { ?>
			<div class="col-md-6 text-center" style='padding-bottom:20px'>
				<A href="<?= Router::instance()->generate('avatars/create/id',array('id'=>$typeValue))?>">
					<div class="img-thumbnail">
						<img src="<?= Router::instance()->generate('avatar/preview-cat/id',array('id'=>$typeValue))?>"> <BR>
						<span class='title'><?=$typeName?></span>
					</div>
				</A>
			</div>
		<? } ?>

		<div class='col-md-12'> <BR><BR> </div>

<? /*


		<div class="row">
			<div class="col-md-7">
				<div class="btn-group btn-group-vertical btn-block highlight-colored">
					<? foreach($children as $child_id=>$child) { ?>
						<A style="margin:20px" onMouseOver="src(undefined,<?=$child_id?>);" onMouseOut="src();" class="btn btn-default btn-block" href="<?= Router::instance()->generate('avatars/create/id',array('id'=>$child_id))?>"><?=$child->name?></A>
					<? } ?>
					<? foreach($assets as $child_id=>$child) { ?>
						<A style="margin:20px" onMouseOver="src(<?=$child_id?>,undefined);" onMouseOut="src();" class="btn btn-default btn-block" href="<?= Router::instance()->generate('avatars/create/id/asset',array('id'=>$category->id,'asset_id'=>$child_id))?>"><?=$child->name?></A>
					<? } ?>
				</div>
			</div>

			<div class="col-md-5 text-center">
				<div style="position:relative;height:<?=$format->height?>px;width:<?=$format->widtdh?>px;">
					<img id="preview" src="<?= Router::instance()->generate('avatar/preview') ?>">
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<BR>
		</div>
	</div>


<script>
	function preload(arrayOfImages) {
		$(arrayOfImages).each(function(){
			$('<img/>')[0].src = this;
		});
	}
	preload([
		<?
			foreach($children as $child_id=>$child)	echo "'".Router::instance()->generate('avatar/preview-cat/id',array('id'=>$child_id))."?t='+cachebuster,";
			foreach($assets as $child_id=>$child)	echo "'".Router::instance()->generate('avatar/preview/id',array('id'=>$child_id))."',";
		?>
	]);
</script>
*/ ?>

<?
	include("footer.php");
?>