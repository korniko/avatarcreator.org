<fieldset>
	<legend> <?=$category->name?> <?=$asset->name?> <span class='pull-right'></span></legend>
	<?
		if (count($roll_options) > 0) { ?>
			<div class='text-center'>
			<div class="btn-group" data-toggle="buttons" style="margin: 0px auto ">
			<?
			foreach($roll_options as $roll_option) {
				if (in_array($roll_option->id, $edit_config[$asset->id]['rids'])) {
					$status = 'active';
				} else {
					$status = '';
				}
				if (Flags::isFlag($roll_option->flags, model_rolls::SINGLE)) {
					?>
					<label class="btn btn-primary <?= $status ?>">
						<input type="radio" name="options"
						       onChange="doAsset(<?= $asset->id ?>,1,<?= $roll_option->id ?>)"
						       id="roll_option_<?= $roll_option->id ?>"> <?= $roll_option->name ?>
					</label>
				<?
				} elseif (Flags::isFlag($roll_option->flags, model_rolls::OPTIONAL)) { ?>
					<label class="btn btn-primary <?= $status ?>">
						<input type="checkbox" name="options"
						       onChange="doAsset(<?= $asset->id ?>,1,<?= $roll_option->id ?>)"
						       id="roll_option_<?= $roll_option->id ?>"> <?= $roll_option->name ?>
					</label>
				<? }
			}
			?>
			</div>
			</div>
			<HR>
			<?
		}
	?>
</fieldset>
<script>
	$('.asset.selected').removeClass('selected'); //Clear plate
	<?php
	 //Build back up
	 foreach($chosen as $asset_id) {
	    echo "$('#asset_{$asset_id}').addClass('selected');\n";
	 }
	 if ($doPreview) { ?>
			preloadimages([<?=$preview_images?>]).done(function(images){
				$('#preview').html('<?=$preview_html?>');
				$('#spinner').hide();
			})
			if ($('#save.btn').hasClass('disabled')) {
				$('#save.btn').removeClass('disabled');
				$('#save.btn').removeClass('btn-default');
				$('#save.btn').addClass('btn-primary');
			}
	<? } ?>
</script>
