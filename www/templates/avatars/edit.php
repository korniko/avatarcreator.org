<?
	include("header.php");
?>
	<link rel="stylesheet" href="/assets/css/spinner.css" type="text/css">
	<link rel="stylesheet" href="/assets/css/steps.css" type="text/css">

<style>
	.assetTree .accordion-group .accordion-toggle .indicator:after {
		font-family: FontAwesome;
		content:  "\f056";
		float: left;
		color: grey;
	}
	.assetTree .accordion-group .accordion-toggle.collapsed .indicator:after {
		content: "\f055";
	}
	.assetTree .panel {
		margin-bottom:0px;
		font-size:22px;
	}
	.assetTree .accordion-group {
		padding-left:15px;
	}

	.assetTree .accordion-heading {
		padding-left:3px;
	}

	.assetTree .indicator {
		padding:3px;
	}

	.asset {
		padding-left:15px;
	}

	.asset-choice {
		background-color:#faf2cc;
		text-align:left;
		font-size:17px;
		width:90%;
	}
	.selected .asset-choice {
		background-color:#FAA732;
	}

	.asset-choice-cog {
		background-color:#faf2cc;
		text-align:left;
		font-size:18px;
		width:10%;
		display:none;
	}
	.selected .asset-choice-cog {
		background-color: #ffbf71;
		display:block;
	}

	.asset-choice-price {
		background-color:#faf2cc;
		text-align:left;
		font-size:12px;
		line-height:24px;
		width:10%;
		display:block;
	}
	.selected .asset-choice-price {
		background-color: #ffbf71;
		display:none;
	}

	.asset-choice:before {
		font-family: FontAwesome;
		display: inline-block;
		content: "\f096";
	}

	.selected .asset-choice:before {
		content: "\f046";
	}

	#previewBox {
		margin:0px auto;
		position:relative;
		height:<?=$avatar->format->height?>px;
		width:<?=$avatar->format->width?>px;
	}

	#save {
		font-size:24px;
	}

	.icon {
		background-image:url('/assets/asset-icons.jpg');
		height:30px;
		width:30px;
		padding-left:30px;
		display:inline-table;
		background-position:-422px;
	}

	.icon.head { background-position:0px; }
	.icon.eyes { background-position:-30px; }
	.icon.nose { background-position:-60px; }
	.icon.eyebrows { background-position:-96px; }
	.icon.face { background-position:-128px; }
	.icon.hair { background-position:-165px; }
	.icon.clothes { background-position:-203px; }
	.icon.body { background-position:-503px; }
	.icon.age { background-position:-422px; }

	.icon.color { background-position:-422px; }
	.icon.lips { background-position:-270px; }


	#previewBox #spinner {
		z-index:100;
		margin:0px auto;
		text-align:center;
		position:relative;
		top:55px;
	}
	#loader {
		font-family: 'Sigmar One', cursive;
		font-size:32px;
		color: white;
		text-shadow:
		-2px -2px 0 #1e9de3,
		2px -2px 0 #1e9de3,
		-2px 2px 0 #1e9de3,
		2px 2px 0 #1e9de3;
	}
</style>
	<script>
		function doAsset( aid, doToggle, rollIndex ) {
			if ( doToggle == 1) {
				$('#spinner').show();
			}
			$.ajax({
				type: "POST",
				url: "/asset/config/<?=$avatar->id?>/"+aid,
				data: { toggle: doToggle, roll:rollIndex }
			}).done( function( data ) {
				$('#config').html( data );
			});
		}

		function afterLoad() {
			$('#spinner').hide();
		}
		function save() {
			$.ajax({
				url:"<?=Router::instance()->generate('avatar/save/id',array('id'=>$avatar->id));?>"
			});
			$('#save.btn').removeClass('btn-primary');
			$('#save.btn').addClass('btn-default');
			$('#save.btn').addClass('disabled');
		}
	</script>


	<div class='container'>
		<H2> Avatar Editor</H2>
		<HR>
		<div class='col-md-5' >
			<div id="previewBox">
				<div class="spinner" id="spinner">
				<span id="loader">LOADING</span>
					<div class="spinner-container container1">
						<div class="circle1"></div>
						<div class="circle2"></div>
						<div class="circle3"></div>
						<div class="circle4"></div>
					</div>
					<div class="spinner-container container2">
						<div class="circle1"></div>
						<div class="circle2"></div>
						<div class="circle3"></div>
						<div class="circle4"></div>
					</div>
					<div class="spinner-container container3">
						<div class="circle1"></div>
						<div class="circle2"></div>
						<div class="circle3"></div>
						<div class="circle4"></div>
					</div>
				</div>
				<div id="preview" style="height:300px;width:300px;">
				</div>
				<? /* <img id="preview" onLoad="afterLoad();" class="img-responsive" src="<?=Router::instance()->generate('avatars/view/id',array('id'=>$avatar->id))?>"> */ ?>
			</div>
			<BR>
			<p>
				<button id="save" onClick="save();" class="disabled btn btn-block btn-default"> <i class="fa fa-save"></i> Save </button>
			</p>
			<BR>
			<fieldset>
				<div id="config">

				</div>
			</fieldset>
		</div>

		<div class="col-md-7 assetTree" style='height:600px;overflow-y:scroll'>
				<?
					function treeWalk($tree,$avatar,$parent_category_id=0) {
						$sysconfig=Config::instance();
						if (isset($_SESSION['edit_config'][$avatar->id])) {
							$edit_config = $_SESSION['edit_config'][$avatar->id];
						} else $edit_config = array();
						if (is_array($tree)) {
							foreach ($tree as $category_id => $childs) {
								if (is_numeric($category_id)) { //Category
									$last_category_id = $category_id;
									$category = new model_categories($category_id);
									?>

									<div class='accordion' id='accordion_<?=$category_id?>'>
										<div class='accordion-group'>
											<div class='panel'>
												<a class='accordion-toggle collapsed' data-toggle='collapse' data-parent='<?=($category->parent_id==1?'.assetTree':'#accordian_'.$category->parent_id)?>' href='#collapse_<?=$category_id?>'>
													<div class='accordion-heading'>
														<span class='indicator'></span>
														<div class='icon <?=Utils::slugify($category->name)?>'></div>
														 <?=$category->name?>
													</div>
												</a>
												<div class='accordion-body collapse' id='collapse_<?=$category_id?>'>
													<?
													foreach ($childs as $child_id => $limb) {
														treeWalk(array($child_id => $limb), $avatar, $category_id); //Recurse
													}
													?>
												</div>
											</div>
										</div>
									</div>
									<?

								} elseif ($category_id = 'assets') {
									foreach ($childs as $asset_id => $config) {
										$asset = new model_assets($asset_id);
										$selected = in_array( $asset_id, array_keys($edit_config) );
									?>
											<div id="asset_<?=$asset_id?>" class='asset btn-group btn-block <?= $selected ? 'selected' : '' ?>'>
												<a onclick="doAsset( <?= $asset->id; ?>, 1);" class="asset-choice btn"> <?= $asset->name ?> </a>
												<a onclick="doAsset( <?= $asset->id; ?>, 0);" class="btn asset-choice-cog"> <i class='fa fa-cog'></i> </a>
												<a onclick="doAsset( <?= $asset->id; ?>, 1);" class="btn asset-choice-price"> <?=$sysconfig["symbols"]["currency"]?> <?=$asset->cost?> </a>
											</div>
									<? }
								}
							}
						}
					}

					treeWalk($tree,$avatar);

					//$parent_category = 0;
					//foreach($category->children( model_categories::CHILDREN_REQUIRED_FIRST ) as $child) include("category-panel.php");
				?>
		</div>
	</div>

	<script>
		$(document).ready(function() {
			$('#spinner').hide();
			loadAv();
		});

		$(document).on('click', '.accordion-toggle', function(event) {
			event.stopPropagation();
			var $this = $(this);

			var parent = $this.data('parent');
			var actives = parent && $(parent).find('.collapse.in');

			// From bootstrap itself
			if (actives && actives.length) {
				hasData = actives.data('collapse');
				//if (hasData && hasData.transitioning) return;
				actives.collapse('hide');
			}

			var target = $this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, ''); //strip for ie7

			$(target).collapse('toggle');
		});

		function loadAv() {
			$('#spinner').show();
			$.ajax({
				type: "POST",
				url: "/asset/config/<?=$avatar->id?>"
			}).done( function( data ) {
				$('#config').html( data );
				$('spinner').hide();
			});
		}

		function preloadimages(arr){
			var newimages=[], loadedimages=0
			var postaction=function(){}
			var arr=(typeof arr!="object")? [arr] : arr
			function imageloadpost(){
				loadedimages++
				if (loadedimages==arr.length){
					postaction(newimages) //call postaction and pass in newimages array as parameter
				}
			}
			for (var i=0; i<arr.length; i++){
				newimages[i]=new Image()
				newimages[i].src=arr[i]
				newimages[i].onload=function(){
					imageloadpost()
				}
				newimages[i].onerror=function(){
					imageloadpost()
				}
			}
			return { //return blank object with done() method
				done:function(f){
					postaction=f || postaction //remember user defined callback functions to be called when images load
				}
			}
		}


	</script>
<!--
<?= var_dump($avatar->rollsArray()); ?>
-->
<?
	include("footer.php");
?>