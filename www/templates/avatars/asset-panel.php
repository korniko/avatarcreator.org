<?
	global $avatar;
	if ($avatar->config_isset($asset->id)) {
		$modify_class = " btn-danger";
	} else {
		$modify_class = "";
	}
	if ($asset->flags & model_assets::OPTIONAL) $modify_class .= " btn-success";

?>
<button onclick="doPreview( <?=$asset->id;?>, this );" class="btn btn-block <?=$modify_class?>"> <?=$asset->name?> / <?=$asset->category_id?> </button>