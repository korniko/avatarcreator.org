<?
$bread = array();
if (!isset($glue)) $glue = " <i class='fa fa-angle-right'></i> ";
foreach($breadcrumb as $crumb) {
	list($name,$url) = $crumb;
	if (!is_null($url)) {
		$bread[] = "<a href='{$url}'>{$name}</a>";
	} else {
		$bread[] = $name;
	}
}
echo join($bread,$glue);
?>