<?php
$meta['title'] = "About ".SITE_NAME;
include("header.php");
?>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<H1>Gallery</H1>
				<P>
					A gallery of nine avatars chosen from our database at random. Each one of these avatars represents a users account. Each account is given three avatars for free. </p>
				</P>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<div class="block-text text-center">
					<img src="http://i.<?=URL_BASE?>/random.gif?1">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="block-text text-center">
					<img src="http://i.<?=URL_BASE?>/random.gif?2">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="block-text text-center">
					<img src="http://i.<?=URL_BASE?>/random.gif?3">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="block-text text-center">
					<img src="http://i.<?=URL_BASE?>/random.gif?4">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="block-text text-center">
					<img src="http://i.<?=URL_BASE?>/random.gif?5">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="block-text text-center">
					<img src="http://i.<?=URL_BASE?>/random.gif?6">
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-sm-4">
				<div class="block-text text-center">
					<img src="http://i.<?=URL_BASE?>/random.gif?7">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="block-text text-center">
					<img src="http://i.<?=URL_BASE?>/random.gif?8">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="block-text text-center">
					<img src="http://i.<?=URL_BASE?>/random.gif?9">
				</div>
			</div>
		</div>

		<BR>
	</div>
	<BR>
<?
include("footer.php");
?>
