<?php
	include("header.php");
?>
	<div class='container'>
		<BR>
		<form method="POST" class="form-horizontal">
				<fieldset>
					<legend>Register</legend>
				<div class='col-md-12 text-center'>
					<p><a href="#" onClick="logInWithFacebook()"><img src="/assets/images/facebook-register-button.png"/></a></p>
					<I>- or -</I> <BR><BR>
				</div>

				<div class='col-md-8'>
					<? if (isset($message)) { ?>
						<div class="alert alert-warning" role="alert"><?=$message?></div>
					<? } ?>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="textinput">Email Address</label>
						<div class="col-md-6">
							<input id="textinput" name="email" placeholder="Your Email Address" class="form-control input-md" type="text">
							<span class="help-block">Your E-mail address</span>
						</div>
					</div>

					<!-- Password input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="passwordinput">Password</label>
						<div class="col-md-6">
							<input id="passwordinput" name="password" placeholder="Password" class="form-control input-md" type="password">
							<span class="help-block"> Password to protect your account</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="passwordinputB">Password Again</label>
						<div class="col-md-6">
							<input id="passwordinputB" name="password_confirmation" placeholder="Password Again" class="form-control input-md" type="password">
							<span class="help-block"> Confirm your password </span>
						</div>
					</div>
					<button class='btn btn-primary btn-block'> Register </button>
					<BR>
				</div>
				<div class='col-md-4'>
					<img src="http://i.<?=URL_BASE?>/random.gif" style='width:100%'>
				</div>

				</fieldset>
		</form>
		<BR>
	</div>
	<BR>
<?
	include("footer.php");
?>
