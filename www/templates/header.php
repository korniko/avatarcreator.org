<!DOCTYPE html>
<html>
<head>
	<title><?= (isset($meta['title']) ? $meta['title'] : (SITE_NAME.' - Custom Avatar Creation and Hosting Engine')) ?></title>

	<meta charset="UTF-8">

	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

	<link rel="icon" href="/images/icon.png">
	<link rel="apple-touch-icon" href="/images/button.png">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


	<!-- Site -->
	<link rel="stylesheet" href="/assets/css/yeti.css" type="text/css">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/assets/css/style.css" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700italic' rel='stylesheet' type='text/css'>
<?php if ($forum) { ?>
	<link rel="stylesheet" href="/<?php echo $template_path . "/styles.css"; ?>" type="text/css">
	<script type="text/javascript" src="/sbb/js/jquery.tipsy.js"></script>
	<link rel="stylesheet" href="/sbb/css/tipsy.css" type="text/css" />
	<script type="text/javascript" src="/sbb/js/sbb.js"></script>
	<?php echo $sbb_add2head; ?>
<?php } ?>

	<script src="/assets/js/script.js"></script>
	<script src="/assets/js/timeago.js"></script>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-54365028-1', 'auto');
		ga('send', 'pageview');

	</script>
</head>

<body>
<script src="/assets/js/facebook.js"></script>
<? include("navbar.php"); ?>
<div id="box" class="broun-block" style="margin-top:45px">
