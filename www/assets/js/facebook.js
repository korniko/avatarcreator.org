logInWithFacebook = function() {
	FB.login(function(response) {
		if (response.authResponse) {
			//Redirect to main login page
            window.location.replace("/fblogin");
		} else {
			alert('User cancelled login or did not fully authorize.');
		}
	}, {scope: 'email,user_likes'});
	return false;
};

logOutWithFacebook = function() {
	FB.logout(function(response) {

	});
}

window.fbAsyncInit = function() {
	FB.init({
		appId      : '678900002214797',
		xfbml      : true,
		cookie     : true,
		version    : 'v2.3'
	});
};

(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));