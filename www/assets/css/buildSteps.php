#!/usr/bin/php -q
<?php
$max = 40;
$width = 300;
for ($i=1;$i<=$max;$i++) { ?>
	/* ------------------------ Step <?=$i?> ----------------------- */
	@-webkit-keyframes framed-<?=$i?> {
	from { background-position: 0px; }
	to { background-position: -<?=$i*$width?>px; }
	}
	@-moz-keyframes framed-<?=$i?> {
	from { background-position: 0px; }
	to { background-position: -<?=$i*$width;?>px; }
	}
	@keyframes framed-<?=$i?> {
	from { background-position: 0px; }
	to { background-position: -<?=$i*$width;?>px; }
	}
	.framed-<?=$i?> {
	-webkit-animation: framed-<?=$i?> 4s steps(<?=$i?>, end) infinite;
	-moz-animation: framed-<?=$i?> 4s steps(<?=$i?>, end) infinite;
	animation: framed-<?=$i?> 4s steps(<?=$i?>, end) infinite;
	}
	/* ------------------------------------------------------------- */


<? }
?>