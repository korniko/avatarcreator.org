<?php
define('SITEKEY','ndFbjoanEmf3KEvRUKHvLNstAywXm7mX'); //For the cryptions and licence valdation
define('DEVEL',TRUE);

//Define library path
set_include_path( get_include_path() .	PATH_SEPARATOR . __DIR__ . PATH_SEPARATOR . __DIR__ . DIRECTORY_SEPARATOR . "libs" );

//Initiate Autoloader
require_once("autoload.php");
require_once("libs/Facebook/autoload.php");

//Initiate Instances through Singleton Factories
$config = Config::instance('default','./config.php');

$db = DB::instance('default',$config['db']['dsn'],$config['db']['user'],$config['db']['password']);
$cache = Cache::instance('default',$config["cache"]["host"],$config["cache"]["port"]);
$scope = Scope::instance('default',$config["paths"]["templates"]);
if ($_SERVER['HTTP_HOST'] == 'i.'.URL_BASE) {
	$router = Router::instance('html',include 'routes-viewer.php');
} else {
	$router = Router::instance('html',include 'routes.php');
}

FKeeper::instance('avatars',$config["paths"]["avatars"]);
FKeeper::instance('rolls',$config["paths"]["rolls"]);
FKeeper::instance('frames',$config["paths"]["frames"]);

session_set_cookie_params(0, '/', '.'.TLD);
session_start();

//Load Facebook
$fb = new Facebook\Facebook([
	'app_id' => $config['facebook']['app_id'],
	'app_secret' => $config['facebook']['app_secret'],
	'default_graph_version' => 'v2.3',
]);
if (isset($_SESSION['FBAT'])) { //If user is logged in via FB
	$fb->setDefaultAccessToken((string)$_SESSION['FBAT']);
}

//Manage Global Scope
controllers_Global::main( $scope );

try {
	$route = $router->match();
	if (isset($route) and ($route !== false)) {
		$scope->inject( array("route"=>$route["name"],"routeSlug"=>Utils::slugify($route["name"])) ); //Inject Route into global vars
		if ($router->execute( $route ) === FALSE) {
			header('X-Error: False Route');
			header("HTTP/1.0 404 Not Found");
			echo Scope::instance()->render('404');
		}
	} else {
		header('X-Error: Missing Route');
		header("HTTP/1.0 404 Not Found");
		echo Scope::instance()->render('404');
	}
} catch ( Exception $e ) {
	echo Scope::instance()->render( 'error', array( "error" => $e->getMessage() ) );
}

?>
