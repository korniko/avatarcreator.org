<?php
	//Common bitwise operations
	class Flags {
		static function isFlag($val,$flag) {
			return ($flag & $val);
		}
		static function setFlag($val,$flag) {
			return ($flag | $flag);
		}
		static function unsetFlag($val,$flag) {
			return ($val & ( ~ $flag ));
		}
	}
?>