<?
    /*
        Array Wheel
    
        Creates an indefinably traversable array

        Copyright 2011 Dan Morgan
        Please check out my website for more useful classes, functions and tools
        http://www.danmorgan.net
    */
    class ArrayWheel {
        var $contents = array();
        var $index=-1;

        function _construct($init=NULL) {
            if (!is_null($init)) {
                foreach($init as $k=>$v) {
                    $this->add($v);
                }
            }
        }

        function new_contents($contents) {
            $this->contents = $contents;
            ksort($this->contents);
        }

        function next() {
            $this->index++;
            if ($this->index > (count($this->contents)-1)) {
                $this->index = 0;
            }
            $out = $this->contents[$this->index];
            return $out;
        }
        
        function prev() {
            $this->index--;
            if ($this->index > 0) {
                $this->index = (count($this->contents)-1);
            }
            $out = $this->contents[$this->index];
            return $out;
        }

        function reset() {
            $this->index = 0;
        }

        function turn_to($element) {
            for($i=0;$i<=(count($this->contents)-1);$i++) {
                if ($element == $this->contents[$i]) {
                    $this->ccw($i);
                    return true;
                }
            }
            return false;
        }

        function sanitize_position($position) {
            $amax = count($this->contents);
            $max = $amax -1;
            if ($position > $max) {
                $oposition = $position % $max;
                $oposition--;
            } else {
                $oposition = $position;
            }
            return $oposition;
        }

        function add($element,$position=-1) {
            if ($position == -1) {
                $this->contents[count($this->contents)] = $element;
            } else {
                $nposition = $this->sanitize_position($position);
                $this->ccw($nposition);
                $this->contents[count($this->contents)] = $element;
                $this->cw($nposition+1);
            }
        }

        function get($position) {
            $nposition = $this->sanitize_position($position);
            return $this->contents[$nposition];
        }
        
        function remove($position=-1) {
            if ($position == -1) {
                $position = (count($this->content)-1);
            }
            $ncontent = $this->contents;
            $nposition = $this->sanitize_position($position);
            $ocontent = array();
            unset($ncontent[$nposition]);
            $mi=0;
            foreach($ncontent as $k=>$v) {
                $ocontent[$mi] = $v;
                $mi++;
            }
            $this->new_contents($ocontent);
        }

        function ccw($steps=1) {
            $ncontents = array();
            for($step=0;$step<$steps;$step++) {
                for($i=0;$i <= (count($this->contents)-2);$i++) {
                    $ncontents[$i] = $this->contents[$i+1];
                }
                $ncontents[count($this->contents)-1] = $this->contents[0];
                $this->new_contents($ncontents);
            }
            
        }

        function cw($steps=1) {
            $ncontents = array();
            for($step=0;$step<$steps;$step++) {
                for($i=0;$i <= (count($this->contents)-2);$i++) {
                    $ncontents[$i+1] = $this->contents[$i];
                }
                $ncontents[0] = $this->contents[count($this->contents)-1];
                var_dump($ncontents);
                $this->new_contents($ncontents);
            }
        }
    }
?>