<?php
	class Utils {
		static function slugify($string){
			return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
		}

		static function genRandomString($length = 50) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
			$string = '';
			for ($p = 0; $p < $length; $p++) {
				$string .= $characters[mt_rand(0, strlen($characters) -1)];
			}
			return $string;
		}

		static function hashData($data) {
			return hash_hmac('sha512',$data, \SITEKEY );
		}

	}
?>