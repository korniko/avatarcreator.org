<?php
/**
 * Singleton with configuration info
 *
 * Config::setFile('config.php'); // Sets configuration file
 * $config = Config::getInstance(); // Portable config via Singleton
 *
 * @author Dan Morgan - danmorgan.net
 *
 */

class Config implements ArrayAccess, Countable, IteratorAggregate {
	protected $_values = array();
	protected $_configFile = '';

	protected static $instances;
	protected static $current_instance='default';

	public static function instance($name=NULL,$filePath=NULL) {
		if (is_null($name)) {
			$name = self::$current_instance;
		} else {
			self::$current_instance = $name;
		}
		if ( ! isset(self::$instances[$name])) {
			self::$instances[$name] = new self($filePath);
		}
		return self::$instances[$name];
	}

	protected function __construct( $filePath ) {
		$this->_configFile = $filePath;
		$values = @include( $this->_configFile );
		if (is_array($values)) {
			$this->_values = &$values;
		}
	}

	final protected function __clone() {}


	public function count() {
		return sizeof($this->_values);
	}

	public function export() {
		return $this->_values;
	}

	public function offsetExists($offset) {
		return array_key_exists($offset, $this->_values);
	}

	public function offsetGet($offset) {
		return $this->_values[$offset];
	}

	public function offsetSet($offset, $value) {
		$this->_values[$offset] = $value;
	}

	public function offsetUnset($offset) {
		unset($this->_values[$offset]);
	}

	public function getIterator() {
		return new ArrayIterator($this->_values);
	}
	public function __set($key, $value) {
		$this->_values[$key] = $value;
	}
	public function __get($key) {
		return $this->_values[$key];
	}
}
?>