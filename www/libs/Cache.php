<?php
class Cache {

	static $instances;
	static $current_instance;
	static $options=array();

	static function instance( $name=NULL, $host=NULL, $port=NULL) {
		if ( ! is_null($name)) {
			if ( ! is_null($host)) {
				static::$options[$name] = array('host'=>$host,'port'=>$port);
			}
			static::$current_instance = $name;
		} else {
			$name = static::$current_instance;
		}

		$memcache = new Memcache;
		$memcache->connect(static::$options[$name]['host'],static::$options[$name]['port']);
		return $memcache;

	}
}
?>