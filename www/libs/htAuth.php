<?php
	class htAuth {
		static function check( $users=NULL ) {
			if (is_null($users)) {
				$config = Config::instance();
				$users = $config["admins"];
			}
			if (isset($_SESSION["user"])) return true;
			$authed = FALSE;
			if (isset($_SERVER['PHP_AUTH_USER'])) {
				if (in_array(strtolower($_SERVER['PHP_AUTH_USER']),array_keys($users))) {
					if (strlen($_SERVER['PHP_AUTH_PW']) > 1) {
						if ($users[ $_SERVER['PHP_AUTH_USER'] ] == $_SERVER['PHP_AUTH_PW']) {
							$authed = true;
						}
					}
				}
			}
			if ($authed == FALSE) {
				header('WWW-Authenticate: Basic realm="AvatarCreator.org"');
				header('HTTP/1.0 401 Unauthorized');
				echo 'You must authenticate to access this area.';
			exit;
			} else {
				$_SESSION["user"] = $_SERVER['PHP_AUTH_USER'];
				return true;
			}
		}
	}
?>