<?php
	class ac_frameConvert {
		static function convert($filename,$format) { //Facade
			$ext = '';
			switch (self::getFileMimeType($filename)) {
				case 'image/png' : $ext = 'PNG'; break;
				case 'image/gif' : $ext = 'GIF'; break;
				case 'image/jpg' : $ext = 'JPG'; break;
				case 'image/jpeg' : $ext = 'JPG'; break;
				default : $ext = ''; break;
			}
			if ($ext == '') return false;
			$target = array('ac_frameConvert'.$ext,'_convert');
			if (is_callable($target)) return call_user_func($target,$filename,$format);

			return false;
		}

		static function getFileMimeType($file) {
			if (function_exists('finfo_file')) {
				$finfo = finfo_open(FILEINFO_MIME_TYPE);
				$type = @finfo_file($finfo, $file);
				finfo_close($finfo);
			} else {
				require_once 'upgradephp/ext/mime.php';
				$type = mime_content_type($file);
			}

			if (!$type || in_array($type, array('application/octet-stream', 'text/plain'))) {
				$secondOpinion = exec('file -b --mime-type ' . escapeshellarg($file), $foo, $returnCode);
				if ($returnCode === 0 && $secondOpinion) {
					$type = $secondOpinion;
				}
			}

			return $type;
		}

		static function convertDestroy($converter) {
			if (is_array($converter)) {
				foreach($converter as $convert) self::convertDestroy($convert);
			} else {
				if (file_exists($converter)) @unlink($converter);
			}
		}

	}
?>