<?php
	class ac_frameConvertPNG extends ac_frameConvert {
		function _convert($filename,$format,$fit=false) {
			$tmpfname = tempnam(sys_get_temp_dir(),TMP_NAME_PREFIX);
			$resize = new Imagick();
			$resize->readImage($filename);
			if ($fit) {
				$resize->scaleImage($format->height, $format->width, true);
			}
			$resize->setImageFormat("png");
			$resize->writeImage($tmpfname);
			return $tmpfname;
		}
	}
?>