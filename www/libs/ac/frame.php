<?php
/**
 * Class ac_frame - Frame is a single animation or image
 *
 * GIF to PNGs: convert   -delay 20   -loop 0   frame*.png   animated.gif
 *
 * PNGs to GIF: convert -delay 20 -loop 0 frame*.png -background white -flatten animated.gif
 *
 */

class ac_frame {

	var $frames;
	var $index=0;

	function __construct( $id=NULL ) {
		$this->frames = new ArrayWheel();
		if ( ! is_null($id)) {
			$sql = "SELECT * from ac_frames WHERE id = {$id}";
		}
	}

	function importGIF($filename) {

	}

	function addImage($filename) {
		$element = $filename; //Load Asset
		$this->frames->add($element);
	}

	function removeImage($index) {
		$this->frames->remove($index);
	}

	function iterate() {
		$obj = $this->frames->get( $this->index );
		$this->index = $this->frames->sanitize_position( $this->index + 1 );
		return $obj;
	}

}

?>