<?php
class ac_frameConvertGIF extends ac_frameConvert {
	function _convert($filename,$format,$fit=false) {
		$tmpfname = tempnam(sys_get_temp_dir(),TMP_NAME_PREFIX);
		if ($fit) {
			$command = "/usr/bin/convert {$filename} -resize 300x300^ -extent 300x300 -gravity center -coalesce -dispose previous {$tmpfname}.png"; //Split GIF into PNG frames
		} else {
			$command = "/usr/bin/convert {$filename} -coalesce -dispose previous {$tmpfname}.png"; //Split GIF into PNG frames
		}
		system($command);
		$g = glob("{$tmpfname}-*.png");
		natsort($g);
		$out = array();
		foreach($g as $frame) {
			$out[] = ac_frameConvert::convert($frame,$format); //Feed PNGs back into frame convert
		}
		return $out;
	}
}
?>