<?php
	class Multiples {
		// LCM = Least Common Multiple
		function lcm($n, $m=NULL) {
			if (is_array($n)) return self::lcm_arr($n);
			return $m * ($n/self::gcd($n,$m));
		}

		function gcd($n, $m) {
			$n=abs($n); $m=abs($m);
			if ($n==0 and $m==0)
				return 1;
			if ($n==$m and $n>=1)
				return $n;
			return $m<$n?self::gcd($n-$m,$n):self::gcd($n,$m-$n);
		}

		function lcm_arr($items){
			while(2 <= count($items)) {
				array_push( $items, self::lcm( array_shift( $items ) , array_shift( $items ) ) );
			}
			return reset($items);
		}
	}
?>