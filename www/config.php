<?php
	define('SITEKEY', 'mr5BO95QNmFTk4JS8Uu6PAcKY1m474hw');
	define('APP_BASE','/www/avatarfoundry.com/www/');
	define('URL_BASE','avatarfoundry.com');
	define('SITE_NAME','Avatar Foundry');
	define('SITE_EMAIL','dan@danmorgan.net');
	define('TLD',$_SERVER['HTTP_HOST']);
	define('TMP_NAME_PREFIX','ac_');
	return array(
		"facebook"=>array(
			'app_id'=>'678900002214797',
			'app_secret'=>'8a50af11ae9ab2eb17e52415ff159d9b'
		),
		"symbols"=>array(
			"currency"=>"&curren;"
		),
		"engine"=>array(
			'avatar_types'=>array(
				1=>'Male',
				2=>'Female'
			),
			'no_preview'=>APP_BASE.'assets/no-preview.gif',
			'default_bg'=>1,
			'missing_avatar'=>2,
			'format_default'=>2,
			'starting_category_id'=>1,
			'max_identities'=>3,
			'max_frames'=>40,
			'rebuild_delay'=>1, //How many seconds before rebuild is abandoned
		),
		"register"=>array(
			'verify_email_template'=>'register_email',
			'verify_email_subject'=>'Welcome to Avatar Creator .org',
			'verify_email_sender'=>SITE_NAME.' Management',
			'verify_email_sender_address'=>'info@'.URL_BASE
		),
		"admins"=>array(
			'digdan'=>'testing'
		),
		"meta"=>array(
			"title"=>SITE_NAME." - Create Custom Animated Avatars and More"
		),
		"paths"=>array(
			"templates"=>APP_BASE."templates/",
			"frames"=>dirname(APP_BASE)."/cache/frames",
			"rolls"=>dirname(APP_BASE)."/cache/rolls",
			"avatars"=>dirname(APP_BASE)."/cache/avatars",

		),
		"cache"=>array(
			"host"=>"localhost",
			"port"=>"11211"
		),
		"db"=>array(
			"dsn"=>"mysql:host=localhost;dbname=avatarcreator",
			"user"=>"avatarfoundry",
			"password"=>"hAfnwdhgTHbecmHCdhyiJoWk4zykRbdH"
		),
		"mail"=>array(
			"smtp"=>"smtp.sendgrid.com",
			"port"=>25,
			"user"=>"digdan",
			"password"=>"e6wSeHbbcj4k3b9M"
		)
	);
?>
