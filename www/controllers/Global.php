<?
	class controllers_Global {
		function main( $s = NULL) {
			if (isset($_SESSION['user_id']) and (!is_null($_SESSION['user_id']))) { //Logged in
				$me = model_users::instanceFromSession();
				$s->inject("me",$me);
				$s->inject("config",Config::instance());
			}
		}
	}
?>
