<?php
	class controllers_AdminView {
		function view($params,$route) {
			controllers_Admin::adminCheck();
			$asset = new model_assets($params['id']);
			if (is_null($asset->location_hash) or ( ! file_exists(FKeeper::instance()->hashPath($asset->location_hash)))) {
				$asset->build(); //We can rebuild it!
			}
			if (isset($asset->location_hash)) {
				$asset->viewed();
				header("Content-type: image/gif");
				$fp = fopen(FKeeper::instance()->hashPath($asset->location_hash),"rb");
				fpassthru($fp);
				fclose($fp);
			}
		}

		function build($params,$route) {
			controllers_Admin::adminCheck();
			$asset = new model_assets($params['id']);
			if ((!is_null($asset->location_hash)) and (file_exists(FKeeper::instance()->hashPath($asset->location_hash)))) {
				FKeeper::instance()->delete( $asset->location_hash ); //Remove previous asset
			}
			self::view($params,$route);
		}

		function frame_view($params,$route) {
			controllers_Admin::adminCheck();
			$path = FKeeper::instance()->hashPath($params['hash']);
			$fp = @fopen($path,"rb");
			if (!$fp) die('Unable to open file');
			header("Content-type: image/png");
			fpassthru($fp);
			fclose($fp);
		}

	}
?>
