<?php
class controllers_Contact {
	function main($param,$route) {
		$scope = Scope::instance();

		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$mail = "From : {$_POST['email']}\nMessage : {$_POST['message']}\n\n";
			mail(SITE_EMAIL,SITE_NAME.' Contact',$mail);
			$message = 'Message Sent';
			$scope->inject('message',$message);
		}

		if ($scope->can_render($route)) {
			echo $scope->render($route);
		} else {
			echo "Unable to render template '{$route}'.";
		}
	}
}
?>