<?php
class controllers_Register {

	function post($params,$route) {
		$config = Config::instance();
		$message = $response = NULL;
		$validate = Validate::instance( 'register', $response, $_POST, ['email','password','password_confirmation'] );
		if ( $validate->
			check('email')->
			fancy('E-mail')->
			required()->
			regex('email')->
			check('password')->
			fancy('Password')->
			required()->
			min_length(4)->
			max_length(36)->
			same('password_confirmation')->
			run() ) {

			$new_user = new model_users();
			if ( ! $new_user->setEmail($_POST['email'])) {
				$message = "Email address taken";
			} elseif (! $new_user->setPassword($_POST['password'])) {
				$message = "Bad password";
			} else {
				$new_user->save();
				try {
					$mailOk = self::send_verify_email($new_user);
				} catch (Exception $e) {
					error_log('Unable to send registration mail: '.$e->getMessage());
				}
				header("Location: ".Router::instance()->generate('register_verify_sent'));die();
			}
		} else {
			$message = $response['message'][0];
		}
		return $message;
	}

	function verify($params,$route) {
		$message = NULL;
		if ($user = model_users::instance($params['user_id'])) {
			if ($user->verification == $params['code']) {
				$user->setVerified(true);
				$user->save();
				$_SESSION['user_id'] = $user->id;
				$this->display($params,'register_verify_thanks');
				die();
			} else {
				$message = "Invalid verification code";
			}
		} else {
			$message = "Unable to verify account";
		}
		if ( ! is_null($message)) Scope::instance()->inject('message',$message);
		$this->display($params,$route);
	}


	function display($params,$route) {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			Scope::instance()->inject('message', $this->post($params,$route));
		}
		if (Scope::instance()->can_render($route)) {
			echo Scope::instance()->render($route);
		} else {
			return false;
		}
	}

	function reverify($params,$route=NULL) {
		$user_id = $params['user_id'];
		if ($user = new model_users($user_id)) {
			if ( ! ( $user->flags && model_users::VERIFIED ) ) {
				self::send_verify_email($user);
			}
			$message = "Verification email resent.";
		} else {
			$message = 'Unable to locate account.';
		}
		Scope::instance()->inject('message',$message);
		echo Scope::instance()->render('login');
	}

	static function send_verify_email(model_users $new_user) {
		$config = Config::instance();
		$km = new kmMail($config['mail']['smtp'],$config['mail']['port'],$config['mail']['user'],$config['mail']['password']);
		$old = Scope::$current_instance; //Save for later
		$mail_scope = Scope::instance('verify_email',$config["paths"]["templates"]);
		$mail_scope->inject('code',$new_user->verification);
		$mail_scope->inject('url',Router::instance()->generate('register_verify',array('user_id'=>$new_user->id,'code'=>$new_user->verification)));
		$email = $mail_scope->render($config['register']['verify_email_template']);
		Scope::instance($old); //Use previous
		return $km->send($config['register']['verify_email_sender_address'],$new_user->email,$config['register']['verify_email_subject'],$email);
	}
}
?>