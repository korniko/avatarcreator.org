<?php
	class controllers_Admin {

		function adminCheck() { //Logged in as admin or get das boot
			$pass = false;
			if ($me = model_users::instanceFromSession() and $me->isAdmin()) $pass = true;
			if ( ! $pass ) Router::instance()->redirect('login'); //Only admins allowed
		}

		function categoryMap($params,$route) {
			$dump = model_categories::layerMap();
			$dump = array_reverse($dump); //Top First
			$iter = count($dump)+1;
			$out = "<H1>Z Mapping</H1><P>Z Mapping shows which category assets are layered above or beneath other category assets. Use when troubleshooting category placement.</P>";
			$out .= "<table class='table table-striped'>";
			$out .= "<tr><th>#</th><th>Position Code</th><th>Position</th><th>Category</th></tr>";
			foreach ($dump as $line) {
				$line_parts = explode("-",$line);
				$label = "Unknown";
				switch ($line_parts[1]) {
					case model_rolls::POSITION_ABOVE : $label = "Above"; break; //Default
					case model_rolls::POSITION_BELOW : $label = "Below"; break;
					case model_rolls::POSITION_TOP : $label = "Top"; break;
					case model_rolls::POSITION_BOTTOM : $label = "Bottom"; break;
				}
				$iter--;
				$out .= "<TR><TD>{$iter}</TD><TD>{$line}</TD><TD>{$label}</TD><TD>".model_categories::fullName($line_parts[0])."</TD></TR>";
			}
			$out .= "</table>";

			Scope::instance()->inject('content',$out);
			echo Scope::instance()->render('admin/single');
		}

		function main($params,$route) {
			controllers_Admin::adminCheck();
			$scope = Scope::instance();
			$categories = model_categories::all_categories();
			$stats = array();
			$stats['categories'] = model_categories::count();
			$stats['avatars'] = model_identities::count();
			$stats['assets'] = model_assets::count();
			$stats['formats'] = model_formats::count();

			$scope->inject("stats",$stats);
			$scope->inject("categories",$categories);
			echo $scope->render('admin/front');
		}

		function logout($params,$route) {
			unset($_SESSION["user"]);
			controllers_Admin::adminCheck();
		}

	}
?>
