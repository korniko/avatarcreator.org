<?php
	class controllers_AdminCategories {

		function categories($params,$route) {
			controllers_Admin::adminCheck();

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if (isset($_POST["del"]) AND (is_array($_POST["del"]))) {
					foreach($_POST["del"] as $k=>$v)
						$handle = DB::instance()->exec("DELETE from categories WHERE id = {$k}");
				}
				if (isset($_POST["cmd"]) and $_POST["cmd"] == 'add_category') {

					if (isset($_POST['category_id'])) {
						$new = new model_categories($_POST['category_id']);
					} else {
						$new = new model_categories();
					}
					$new->name = $_POST['name'];
					$new->parent_id = $_POST['parent_id'];
					$new->format_id = $_POST['format_id'];
					$new->created = time();

					//Set Flags & Visibility
					if (isset($_POST['flags']['required'])) $new->flags = $new->flags | model_categories::REQUIRED;
					if (isset($_POST['hide']) and (is_array($_POST['hide']))) {
						foreach($_POST['hide'] as $hideFlag) {
							$new->avatarType = $new->avatarType | $hideFlag;
						}
					}

					$new->save();

				}
				if (isset($_POST['category_id'])) { //Edit
					$category = new model_categories($_POST['category_id']);
					$category->name = $_POST['name'];
					$category->format_id = $_POST['format_id'];
					$category->parent_id = $_POST['parent_id'];
					$category->avatarType = $_POST['avatarType'];
					$category->position = $_POST['position'];
					$category->position_category_id = $_POST['position_category_id'];
					$category->flags = self::getPostFlags();

					//Set Flags & Visibility
					if (isset($_POST['flags']['required'])) $category->flags = $category->flags | model_categories::REQUIRED;
					if (isset($_POST['hide']) and (is_array($_POST['hide']))) {
						foreach($_POST['hide'] as $hideFlag) {
							$category->avatarType = $category->avatarType | $hideFlag;
						}
					}

					$category->save();
				}
			}

			$scope = Scope::instance();

			$scope->inject("formats", model_formats::all_formats() );
			$scope->inject("categories", model_categories::all_categories() );

			echo $scope->render('admin/categories');
		}

		function ajaxEdit($params,$route) {
			$scope = Scope::instance();
			$category = new model_categories($params['id']);
			$format = new model_formats($category->format_id);
			$scope->inject('category',$category);
			$scope->inject('format',$format);
			$scope->inject("formats", model_formats::all_formats() );
			$scope->inject("categories", model_categories::all_categories() );

			echo $scope->render('admin/category-edit-modal');
		}

		function getPostFlags() {
			$flags = 0;
			if (isset($_POST['flags']['required'])) $flags = $flags | model_categories::REQUIRED;
			return $flags;
		}


	}
?>