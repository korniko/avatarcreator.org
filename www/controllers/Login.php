<?php
	class controllers_Login {
		function login($params,$route=NULL) {
			$scope = Scope::instance();
			$message = NULL;
			if ($_SERVER['REQUEST_METHOD'] == "POST") {
				$email = $_POST['email'];
				$password = $_POST['password'];
				$remember = @$_POST['remember'];
				$user = model_users::instanceByEmail($email);
				$g = model_users::login($email,$password,$remember=false);
				switch(model_users::login($email,$password,$remember=false)) {
					case model_users::LOGGED_IN :
						$_SESSION['user_id'] = $user->id;
						header("Location: ".Router::instance()->generate('avatars'));
						die();
						break;
					case model_users::PASSWORD_WRONG : $message = 'Email or Password incorrect.';break;
					case model_users::NOT_VERIFIED : $message = 'Account is not verified. <A HREF="'.Router::instance()->generate("register_reverify",array("user_id"=>$user->id)).'"> Verify Now </A>';break;
					case model_users::NOT_ACTIVE : $message = 'Account is not activated.';break;
					case model_users::LOGIN_ERROR : $message = 'You could not be logged in at this time.';break;
					case model_users::NOT_FOUND : $message = 'Account not found.';break;
				}
			}
			$scope->inject('message',$message);
			if ($scope->can_render($route)) echo $scope->render($route);
		}

		function lost($params,$route=NULL) {
			//User forgot their information
			$scope = Scope::instance();
			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			}

			if ($scope->can_render($route)) echo $scope->render($route);
		}

		function fblogin($params,$route=NULL) {
			global $fb;
			$helper = $fb->getJavaScriptHelper();
			try {
				$accessToken = $helper->getAccessToken();
			} catch (Facebook\Exceptions\FacebookResponseException $e) {
				echo "Graph Error ".$e->getMessage();
				die();
			} catch (Facebook\Exceptions\FacebookSDKException $e) {
				echo "FB ERROR " . $e->getMessage();
				die();
			}

			$response = $fb->get('/me?fields=id,email,first_name,last_name,gender,birthday',$accessToken);
			$fbuser = $response->getGraphUser();
			//Search for fbid in users table
			if ($user = model_users::instanceByFBID($fbuser->getId())) { //FB user account found
				$_SESSION['user_id'] = $user->id;
				header("Location: ".Router::instance()->generate('avatars'));
				die();
			} else { //FB account not yet associated with user record
				$email = $fbuser->getEmail();
				if (is_null($email)) { //Email not provided by FB, lets assume one
					$email = ($fbuser->getFirstName().".".$fbuser->getLastName())."@facebook.com";
					$dia = new Diacritics(); //To normalize foriegn strings
					$email = $dia->removeDiacritics($email);
				}
				if ($user = model_users::instanceByEmail($email)) { //Found email, associate the account
					$user->setFBID($fbuser->getId());
					$user->save();
					$_SESSION['user_id'] = $user->id;
					header("Location: ".Router::instance()->generate('avatars'));
					die();
				} else { //No user associate with fb account, create one.
					$user = new model_users();
					$user->setPassword('');
					$user->setEmail($email);
					$user->setFBID($fbuser->getId());
					$user->setVerified( TRUE );
					$user->save();
					$_SESSION['user_id'] = $user->id;
					header("Location: ".Router::instance()->generate('avatars'));
					die();
				}
			}
		}


		function logout($params,$route=NULL) {
			unset($_SESSION['user_id']);
			unset($_COOKIE['remember']);
			header("Location: /");
			die();
		}

	}

?>
