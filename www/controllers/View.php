<?php
	class controllers_View {

		function info($params,$routes) {
			$config = Config::instance();
			if ($params['slug']) {
				$slug = substr($params['slug'],1);
				if ($bc = base58::decode($slug)) {
					$ident = new model_identities($bc);
					$packed = array(
						'identity'=>array(
							'id'=>$slug,
							'secret'=>md5($bc),
							'createdTimestamp'=>$ident->created,
							'createdDate'=>date('r',$ident->created),
						),
						'sizes'=>array(
							'large'=>'300x300',
							'medium'=>'150x150',
							'small'=>'75x75',
							'tiny'=>'32x32'
						),
						'still'=>array(
							'large'=>'http://i.'.URL_BASE.'/'.$slug.'l.jpg',
							'medium'=>'http://i.'.URL_BASE.'/'.$slug.'m.jpg',
							'small'=>'http://i.'.URL_BASE.'/'.$slug.'s.jpg',
							'tiny'=>'http://i.'.URL_BASE.'/'.$slug.'x.jpg',
						),
						'animated'=>array(
							'large'=>'http://i.'.URL_BASE.'/'.$slug.'l.gif',
							'medium'=>'http://i.'.URL_BASE.'/'.$slug.'m.gif',
							'small'=>'http://i.'.URL_BASE.'/'.$slug.'s.gif',
							'tiny'=>'http://i.'.URL_BASE.'/'.$slug.'x.gif',
						),
					);
					header('Content-Type: application/json');
					echo json_encode($packed);
					die();
				}
			}
		}
	
		function mainStill($params,$routes) {
			$config = Config::instance();
			$force = false;
			if ($params['slug']) {
				$slug = $params['slug'];
				$bslug = substr($slug,0,-1);
				$format = substr($slug,strlen($slug)-1,1);

				if ($bslug == "/rando")	{
					$randomId = model_identities::randomId();
					header("X-AvatarID:{$randomId}");
					$bslug = base58::encode( $randomId );
					$format = "l";
				}

				if ($bc = base58::decode($bslug)) {
					$ident = new model_identities($bc);
					switch($format) {
						case "f":$force=true;
						case "l":$target=$ident->still_lg;break;//300x300
						case "m":$target=$ident->still_md;break;//150x150
						case "s":$target=$ident->still_sm;break;//75x75
						case "x":$target=$ident->still_xs;break;//32x32
						default:break;
					}
					if (isset($target)) {
						$target_path = FKeeper::instance('avatars')->hashPath($target);
						if (file_exists($target_path)) {
							//$asset->viewed();
						} else { //Image not found, display placeholder.
							if ($randomId) return self::mainStill($params,$routes);
							$target_path = $config['engine']['no_preview'];
						}
						header("Content-type: image/jpg");
						if ($force) header('Content-Disposition: attachment; filename="avatar-'.$ident->id.'.jpg"');
						$fp = fopen($target_path,"rb");
						fpassthru($fp);
						fclose($fp);
					}
				} else {
					die('not found');
				}
			} else {
				header("Location: http://".URL_BASE);
			}
		}

		function mainAnimated($params,$routes) {
			$config = Config::instance();
			$force = false;
			if ($params['slug']) {
				$slug = $params['slug'];
				$bslug = substr($slug,0,-1);
				$format = substr($slug,strlen($slug)-1,1);

				if ($bslug == "/rando")	{
					$randomId = model_identities::randomId();
					header("X-AvatarID:".base58::encode($randomId));
					$bslug = base58::encode( $randomId );
					$format = "l";
				}

				if ($bc = base58::decode($bslug)) {
					$ident = new model_identities($bc);
					switch($format) {
						case "f":$force = true;
						case "l":$target=$ident->hash_lg;break;//300x300
						case "m":$target=$ident->hash_md;break;//150x150
						case "s":$target=$ident->hash_sm;break;//75x75
						case "x":$target=$ident->hash_xs;break;//32x32
						default:break;
					}
					if ((strlen($target) == 0) or (is_null($target))) { //Invalid target, blame source identity
						//Remove offending identity
						$user = new model_users($ident->user_id);
						$ident->delete();
                        			$user->calcIdents();
						//Display 404
						$target_path = $config['engine']['no_preview'];
					} else {
						$target_path = FKeeper::instance('avatars')->hashPath($target);
					}
					if (file_exists($target_path)) {
						//$asset->viewed();
					} else { //Image not found, display placeholder.
						if ($randomId) return self::mainAnimated($params,$routes);
						$target_path = $config['engine']['no_preview'];
					}
					header("Content-type: image/gif");
					if ($force) header('Content-Disposition: attachment; filename="avatar-'.$ident->id.'.gif"');
					$fp = fopen($target_path,"rb");
					fpassthru($fp);
					fclose($fp);
				} else {
					die('not found');
				}
			} else {
				header("Location: http://".URL_BASE);
			}
		}

		function randomStill($params,$route) {
			$rand = model_identities::randomId();
			$target = new model_identities(  );
			if (isset($target)) {
				//$asset->viewed();
				header("Content-type: image/jpg");
				$fp = fopen(FKeeper::instance('avatars')->hashPath($target->still_lg), "rb");
				fpassthru($fp);
				fclose($fp);
				die();
			}
		}
		function randomAnimated($params,$route) {
			$target = new model_identities( $rand = model_identities::randomId() );
			if (isset($target)) {
				//$asset->viewed();
				header("Content-type: image/gif");
				$fp = fopen(FKeeper::instance('avatars')->hashPath($target->hash_lg), "rb");
				fpassthru($fp);
				fclose($fp);
				die();
			}
		}


	}
?>
