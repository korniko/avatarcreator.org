<?php
	class controllers_Gallery {
		function main($param,$route) {
			$scope = Scope::instance();
			if ($scope->can_render($route)) {
				echo $scope->render($route);
			} else {
				echo "Unable to render template '{$route}'.";
			}
		}
	}
?>