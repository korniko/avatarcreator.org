<?php
	class controllers_Avatars {
		function main($params,$route) {
			$s = Scope::instance();
			$avatars = array();
			if ($s->value('me')) { //Logged in
				foreach(model_identities::fromUser($s->value('me')->id) as $av) {
					if (!is_null($av)) {
						$av->info['hash_xs'] = FKeeper::instance('avatars')->hashInfo($av->hash_xs);
						$av->info['hash_sm'] = FKeeper::instance('avatars')->hashInfo($av->hash_sm);
						$av->info['hash_md'] = FKeeper::instance('avatars')->hashInfo($av->hash_md);
						$av->info['hash_lg'] = FKeeper::instance('avatars')->hashInfo($av->hash_lg);
						$av->info['still_xs'] = FKeeper::instance('avatars')->hashInfo($av->still_xs);
						$av->info['still_sm'] = FKeeper::instance('avatars')->hashInfo($av->still_sm);
						$av->info['still_md'] = FKeeper::instance('avatars')->hashInfo($av->still_md);
						$av->info['still_lg'] = FKeeper::instance('avatars')->hashInfo($av->still_lg);
					}
					$avatars[] = $av;
				}
				$s->inject('avatars',$avatars);
				$s->inject('config_engine',Config::instance());
				echo Scope::instance()->render('avatars/main');
			} else {
				header("Location: ".Router::instance()->generate('login'));
				die();
			}
		}

		function del($params,$route) {
			$s = Scope::instance();
			$avatar = new model_identities($params['id']);
			if (!$s->exists('me') || $s->value('me')->id != $avatar->user_id) return; //Security
			$avatar->delete();
			$user = new model_users($_SESSION['user_id']);
			$user->calcIdents();
			Router::instance()->redirect('avatars');
		}

		function nextRequired($cat_id) {
			$orig = $cat_id;
			if ( ! isset($_SESSION['create_skip'])) {
				$skip = array();
			} else {
				$skip = $_SESSION['create_skip'];
			}
			$cat = new model_categories($cat_id);
			if ($cat->asset_count() == 0) { //Required cat with no assets = bridging category
				//Use first required child
				foreach ($cat->children(TRUE) as $child) {
					if (! in_array($child->id,$skip)) return self::nextRequired($child->id);
				}
			}
			if ((count($skip) > 0) and ($cat->id == $_SESSION['create_root'])) {
				return -1; //Ready to create
			}
			return $cat->id;
		}

		function create($params,$route) {
			$c = Config::instance();
			$user = new model_users($_SESSION['user_id']);
			$d = $user->calcIdents();

			if ($user->identities >= $c['engine']['max_identities']) Router::instance()->redirect("avatars"); //Enfoce avatar limits
			if ( ! isset($params['id'])) { //Select Root Category
				$cat = new model_categories( $c['engine']['starting_category_id'] );
			} else {
				//Create avatar
				$avatar = new model_identities();
				$avatar->root($params['id']);
				$avatar->user_id = $_SESSION['user_id'];
				if (isset($_SESSION['preview_config'])) {
					$avatar->config = $_SESSION['preview_config'][$params['id']];
				} else {
					$avatar->randomAssets();
				}
				if ($avatar->save()) { //Saved?
					unset($_SESSION['preview_config']);
					$user->calcIdents();
					$user->save();
					Router::instance()->redirect("avatar/edit/id",array("id"=>$avatar->id));
				} else {
					die('Error creating avatar.');
				}
			}

			$s = Scope::instance();
			$s->inject('category',$cat);
			$s->inject('avatarTypes',$c['engine']['avatar_types']);
			$s->inject('assets',$cat->allAssets());
			echo $s->render("avatars/create");
		}

		function preview($params,$route) {
			//View avatar with assets or categories
			$preview = new model_identities(); //tmp preview
			if ($route == "avatar/preview-cat/id") { //Category Preview
				$preview->avatar_type = $params['id'];
				unset($_SESSION['preview_config'][$params['id']]); //Clear current config state
				$preview->randomAssets(NULL,$preview->avatar_type);
				if (!isset($_SESSION['preview_config'])) { //Save preview information
					$_SESSION['preview_config'] = array($params['id'] => $preview->config);
				} else {
					$_SESSION['preview_config'][$params['id']] = $preview->config;
				}
			} elseif ($route == "avatar/preview/id") {
				$preview->config = $_SESSION['edit_config'][$params['id']];
				$preview->avatar_type = $params['id'];
			} elseif ($route == "avatar/preview/roll/id") { //Asset with specific Roll
				$asset = new model_assets($params['asset_id']);
				$category = new model_categories($asset->category_id);
				if (isset($_SESSION['edit_config'][$params['id']])) {
					$preview->config = $_SESSION['edit_config'][$params['id']];
					if (($asset->flags & model_assets::REQUIRED) or ($category->flags & model_categories::REQUIRED)) {
						$preview->config_set($params['asset_id'],array('rid'=>$params['roll_id']));
					} else {
						if ($preview->config_isset( $params['asset_id'] )) { //Toggle
							$preview->config_unset($params['asset_id']);
						} else {
							$preview->config_set($params['asset_id'],array('rids'=>array($params['roll_id'])));
						}
					}
					$_SESSION['edit_config'][$params['id']] = $preview->config;
				}
			} else {
				$preview->config = NULL;
			}
			$preview->buildStep( NULL, TRUE );
			header("Content-type: image/jpg");
			$fp = fopen(FKeeper::instance('avatars')->hashPath($preview->still_lg),"rb");
			fpassthru($fp);
			fclose($fp);
			$preview->purge();
		}

		function build($params,$route) {
			$ident = new model_identities($params['id']);
			$ident->build();
			echo "<IMG SRC=\"/avatars/view/".$params['id']."\">";
		}
		function doBuild($params,$route) {
			$ident = new model_identities($params['id']);
			$ident->buildStep();
		}

		function view($params,$route) {
			$ident = new model_identities($params['id']);
			if (($ident->hash_lg=='') or ( ! file_exists(FKeeper::instance('avatars')->hashPath($ident->hash_lg)))) {
				//$ident->build(); //We can rebuild it!
				header("Content-type: image/gif");
				echo file_get_contents("assets/images/404.jpg");
				die();
			}
			if (isset($ident->hash_lg)) {
				//$asset->viewed();
				header("Content-type: image/gif");
				$fp = fopen(FKeeper::instance('avatars')->hashPath($ident->hash_lg),"rb");
				fpassthru($fp);
				fclose($fp);
			}
		}
		function sview($params,$route) {
			$ident = new model_identities($params['id']);
			if (($ident->still_lg=='') or ( ! file_exists(FKeeper::instance('avatars')->hashPath($ident->still_lg)))) {
				header("Content-type: image/gif");
				echo file_get_contents("http://placehold.it/300x300&text=Building...");
				die();
			}
			if (isset($ident->still_lg)) {
				//$asset->viewed();
				header("Content-type: image/jpg");
				$fp = fopen(FKeeper::instance('avatars')->hashPath($ident->still_lg),"rb");
				fpassthru($fp);
				fclose($fp);
			}
		}

		function viewRoll($params,$route) {
			$roll = new model_rolls($params['id']);
			if ($_SERVER['REQUEST_METHOD'] == "GET") {
				if ($hash = $roll->buildSprite(NULL,true)) {
					$path = FKeeper::instance('rolls')->hashPath($hash);
					$fs = filesize($path);
					header("Cache-Control: public, max-age=".(60*60*3));
					header("Content-Length:{$fs}");
					header("Content-type: image/png");

					$fp = fopen( $path ,"rb");
					fpassthru($fp);
					fclose($fp);
				}
			} else { //Build custom configured roll
				$config = $_POST['config'];
				//TODO Finish custom rolls

			}
		}
		function edit($params,$route) {
			$s = Scope::instance();
			$config = Config::instance();
			$starting_category_id = $config['engine']['starting_category_id'];
			$avatar = new model_identities($params['id']);
			if (!$s->exists('me') || $s->value('me')->id != $avatar->user_id) return; //Security
			$avatar->format = $avatar->format();
			$category = new model_categories( $starting_category_id );
			//Load edit config
			$_SESSION['edit_config'][$params['id']] = $avatar->config;
			//Build map of category/assets
			$tree = model_categories::buildTree($starting_category_id,0,0,$avatar->avatar_type);
			$s->inject('tree',$tree);
			$s->inject('edit_config',$avatar->config);
			$s->inject('avatar',$avatar);
			$s->inject('category', $category );
			echo $s->render("avatars/edit");
		}

		function ajaxConfig($params,$route) { //Asset Config Edit
			$s = Scope::instance(); //Init Scope
			$doPreview = false;
			$rollOptions = array();
			$identity_id = $params['id']; //Define which identity
			$avatar = new model_identities($identity_id); //Load identity
			if (!$s->exists('me') || $s->value('me')->id != $avatar->user_id) return; //Ensure security
			$preview = $avatar; //Temporary identity obj
			$preview->config = $_SESSION['edit_config'][$params['id']]; //Load config from session
			$asset_id = (isset($params['aid']) ? $params['aid'] : false);
			$roll = (isset($_REQUEST['roll'])) ? $_REQUEST['roll'] : false; //Set roll if provided
			$toggle = (isset($_REQUEST['toggle']) and ($_REQUEST['toggle'] == 1)) ? true : false; //Set toggle if provided
			$html = '';//Html to push back to avatar builder
			if (!$asset_id) {
				$doPreview = true;//First load
			} else {
				//Load Roll Options
				$asset = new model_assets( $asset_id );
				$rollOptions = $asset->rollOptions();
				$s->inject('asset', $asset);
				$s->inject('roll_options', $rollOptions);
			}

			if ($toggle) {
				if ($roll) {
					$preview->config_set($asset_id,array($roll=>true));
				} else {
					$preview->config_set($asset_id); //Set/Unset Asset
				}
				$_SESSION['edit_config'][$params['id']] = $preview->config;
				$doPreview = true;

			}

			if ($doPreview) {
				$rolls = $preview->rollsArray();
				ksort($rolls);
				foreach ($rolls as $roll) {
					$roll_url = Router::instance()->generate('roll/view', array('id' => $roll->id));
					$frames = $roll->frame_count();
					$preview_images[] = "'" . $roll_url . "'";
					$html .= "<div class=\"roll_cell framed-{$roll->frame_count()}\" id=\"roll_{$roll->id}\" style=\"background:url(" . $roll_url . ")\"></div>";
				}
			}


			$s->inject('config', Config::instance());
			$s->inject('chosen', array_keys($preview->config));
			$s->inject('preview_images',join(",",$preview_images));
			$s->inject('preview_html',$html);
			$s->inject('avatar', $avatar);
			$s->inject('doPreview', $doPreview);
			$s->inject('edit_config', $preview->config);
			echo $s->render("avatars/config");
			die();

		}

		function editSave($params,$route) { //Save current edit configuration to avatar
			$s = Scope::instance();
			if (isset($params['id']) and (is_numeric($params['id']))) {
				if (isset($_SESSION['edit_config'][$params['id']])) {
					$avatar = new model_identities($params['id']);
					if (!$_SESSION['user_id'] || $_SESSION['user_id'] != $avatar->user_id) return; //Security
					$avatar->config = $_SESSION['edit_config'][$params['id']];
					//unset($_SESSION['edit_config'][$params['id']]);
					$avatar->save();
					$avatar->build();
				} else {
					echo "NO SESSION";
				}
			} else {
				echo "NO AVATAR";
			}
		}

		function ajaxCheck($params,$route) {
			$avatar = new model_identities($params['id']);
			if ($avatar->progress == model_identities::PROGRESS_STILL) { //Stills only
				$img = Router::instance()->generate('avatars/sview/id', array('id' => $avatar->id));
			} else {
				$img = Router::instance()->generate('avatars/view/id',array('id'=>$avatar->id));
			}
			if ($avatar->rebuild > 0) {
				$building = true;
			} else {
				$building = false;
			}
			echo json_encode(array(
				'url'=>$img,
				'stage'=>$building
			));
		}

	}
?>
