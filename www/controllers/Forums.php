<?php
	class controllers_Forums {
		function main($params,$route) {
			$scope = Scope::instance();

			//Fill Session info

/*
	*     $this->auth = $_SESSION["SBBauth"];
	*         $this->user = $_SESSION["user"];
	*                 $this->uid = $_SESSION["SBBuid"];
	*                         $this->gid = $_SESSION["gid"];
	*                                     $this->last_login = $_SESSION["last_login"];
	*                                                 $this->is_mod = $_SESSION["is_mod"];
	*                                                                 $this->is_admin = $_SESSION["is_admin"];
	*                                                                                 $this->is_board_admin = $_SESSION["is_board_admin"];
	*                                                                                                     $this->new_tids = $_SESSION["new_tids"];
	*                                                                                                                     $this->last_visit = $_SESSION["last_visit"];
	*                                                                                                                     */

			if ($me = $scope->value('me')) {
				$_SESSION['SBBauth'] = true;
				$_SESSION['user'] = $me->email;
				$_SESSION['SBBuid'] = $me->id;
				$_SESSION['gid'] = $me->id;
				$_SESSION['is_mod'] = false;
				$_SESSION['is_admin'] = false;
				$_SESSION['is_board_admin'] = false;
				$_SESSION['last_visit'] = $me->last_login;
				switch ($me->email) {
					case "info@avatarfoundry.com" :
						$_SESSION['is_admin'] = true;
						$_SESSION['is_mod'] = true;
						$_SESSION['is_board_mod'] = true;
					break;
				}
			} else {
				$_SESSION['SBBauth'] = false;
			}




			if ($scope->can_render($route)) {
				echo $scope->render($route);
			} else {
				echo "Unable to render template '{$route}'.";
			}
		}
	}
?>
