<?php
	class controllers_AdminAssets {
		function assets($params,$route) {
			controllers_Admin::adminCheck();
			$category_id = $params["id"];
			$category = new model_categories($category_id);
			$scope = Scope::instance();

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["delete"]) {
					if ($asset = new model_assets($_POST["delete"])) {
						$asset->delete();
					}
				} elseif (isset($_POST['category_id'])) { //Edit
					$category = new model_categories($_POST['category_id']);
					$category->name = $_POST['name'];
					$category->format_id = $_POST['format_id'];
					$category->parent_id = $_POST['parent_id'];
					$category->flags = self::getPostFlags();
					$category->avatarType = $_POST['avatarType'];
					$category->save();
				} else {
					//Add Asset
					$asset = new model_assets();
					$asset->created = time();
					$asset->name = $_POST["name"];
					$asset->category_id = $category_id;
					$asset->namespace = strtolower($category->name);
					$asset->flags = self::getPostFlags();
					$asset->save();
				}
			}
			$crumb = array_reverse( model_categories::admin_breadcrumb( $category_id ) );
			$scope->inject('breadcrumb', $crumb);
			$scope->inject("categories",model_categories::all_categories());
			$scope->inject("formats",model_formats::all_formats());
			$scope->inject("category",$category);
			$scope->inject("children",$category->children());
			$scope->inject("assets",$this->load_assets( $category_id ) );

			echo $scope->render("admin/assets");
		}

		function asset($params,$route) {
			controllers_Admin::adminCheck();
			$scope = Scope::instance();

			if ($_SERVER["REQUEST_METHOD"]=="POST") {
				if (isset($_POST["delete"])) {
					$delRoll = new model_rolls($_POST["delete"]);
					$delRoll->delete();
				}
				if (isset($_POST['pcid'])) {
					foreach($_POST['pcid'] as $k=>$v) {
						$roll = new model_rolls($k);
						$roll->position = $_POST['position'][$k];
						$roll->position_category_id = $_POST['pcid'][$k];
						$roll->flags = 0 | @$_POST['roll_flags']['single'][$k] | @$_POST['roll_flags']['optional'][$k];
						if (isset($_POST['roll_namespace'][$k])) $roll->namespace = $_POST['roll_namespace'][$k];
						$roll->name = $_POST['name'][$k];
						$roll->save();
					}
				}
				if (isset($_POST["cmd"])) {
					if ($_POST["cmd"] == "update_asset") {
						$handle = DB::instance()->prepare("UPDATE assets SET name=:name,category_id=:category_id,flags=:flags,cost=:cost,namespace=:namespace WHERE id = :id");
						$handle->execute(array(
							':name'=>$_POST["aname"],
							':category_id'=>$_POST["category_id"],
							':flags'=>self::getPostFlags(),
							':cost'=>$_POST["cost"],
							':namespace'=>$_POST["namespace"],
							':id'=>$params["id"]
						));
					} elseif ($_POST["cmd"] == "add_roll") {
						$roll = new model_rolls();
						$roll->asset_id = $params["id"];
						$roll->save();
					}
				}
			}

			$asset = new model_assets($params["id"]);
			$crumb = array_reverse( model_categories::admin_breadcrumb( $asset->category_id ) );
			$crumb[] = array($asset->name,'#');
			$scope->inject('breadcrumb', $crumb);
			$scope->inject('asset', $asset);
			$scope->inject('rolls', $asset->loadRolls() );
			$scope->inject('format', model_formats::fromType ());
			$scope->inject('category', new model_categories( $asset->category_id ));
			$scope->inject('asset_id',$params['id']);
			$scope->inject("categories",model_categories::all_categories());
			echo $scope->render("admin/asset");
		}

		function asset_order($params,$route) {
			controllers_Admin::adminCheck();
			$order = 1;
			$parent_category_id = $params['id'];
			foreach(explode(",",$_REQUEST['order']) as $category_id) {
				model_categories::setOrder($order++,$category_id);
			}
		}

		function asset_roll($params,$route) {
			controllers_Admin::adminCheck();
			$scope = Scope::instance();
			$roll = new model_rolls($params["id"]);
			$asset = new model_assets($roll->asset_id);
			$category = new model_categories($asset->category_id);
			$format = new model_formats($category->format_id);

			if (isset($_POST["color"]) and (is_array($_POST["color"]))) {
				foreach($_POST["color"] as $frame_id=>$color) {
					if ($color != "") {
						$colored = new model_frames( $frame_id );
						$colored->hue($color);
					}
				}
			}

			if (isset($_POST["remove"])) {
				$target = $_POST["remove"];
				$rem = new model_frames($target);
				$rem->delete();
			}
			if (isset($_POST["duplicate"])) {
				$target = $_POST["duplicate"];
				$dup = new model_frames($target);
				$dup->id = NULL;
				$dup->position = $roll->maxPosition( TRUE );
				$dup->save();
			}
			if (isset($_POST["forward"])) $roll->moveFrame($_POST["forward"],TRUE);

			if (isset($_POST["backward"])) $roll->moveFrame($_POST["backward"],FALSE);

			if (@$_FILES["file"]["size"] > 0) { //Uploaded
				$error = '';
				$target = $_FILES["file"]["tmp_name"];
				$converter = ac_frameConvert::convert($target,$format);
				if (is_array($converter)) {
					foreach($converter as $convert) {
						$roll->frameFromFile( $convert );
					}
				} else {
					if (($frame = $roll->frameFromFile( $converter ) === FALSE)) {
						ac_frameConvert::convertDestroy($converter);
						echo "<meta http-equiv='refresh' content='5'>";
						echo "<B>There was an error!</B><P>Roll will return shortly</P>";
						die();
					}
				}
				ac_frameConvert::convertDestroy($converter);

			}


			$scope->inject("roll_id",$params["id"]);
			$scope->inject("asset_id",$roll->asset_id);
			$scope->inject('format', model_formats::fromType ( ) );
			$scope->inject("frames",$roll->allFrames());
			echo $scope->render("admin/roll");
		}

		protected function load_assets( $category_id  ) {
			controllers_Admin::adminCheck();
			$out = array();
			$handle = DB::instance()->query("SELECT * from categories")->fetchAll();
			foreach($handle as $category)
				$categories[$category["id"]] = $category["name"];

			$handle = DB::instance()->query("SELECT id,category_id from assets WHERE category_id = {$category_id}");

			if ($handle->rowCount() > 0) {
				while($res = $handle->fetch()) {
					$asset = new model_assets($res['id']);
					$asset->category = $categories[$res["category_id"]];
					$asset->roll_count = model_assets::rollCount($res['id']);
					$out[] = $asset;
				}
				return $out;
			} else {
				return array();
			}
		}

		function getPostFlags() {
			$flags = 0;
			if ($_POST["flags"]["required"]) $flags = $flags | model_assets::REQUIRED;
			if ($_POST["flags"]["hidden"]) $flags = $flags | model_assets::HIDDEN;
			if ($_POST["flags"]["optional"]) $flags = $flags | model_assets::OPTIONAL;
			return $flags;
		}

	}
?>